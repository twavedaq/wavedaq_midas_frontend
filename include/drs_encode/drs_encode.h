#ifndef DRS_ENCODE_H
#define DRS_ENCODE_H
#include <math.h>

#if !defined(__GNUC__)
#   if defined(_MSC_VER)
#      define __inline__ __inline
#      define __restrict__
#   else
#      define __inline__ inline
#      define __restrict__
#   endif
#endif

// check if C99 variable length array is available
#if defined(__GNUC__) && !defined(__STRICT_ANSI__)
#   define VLA_AVAILABLE
#endif

// check endian
#if defined(__alpha) || defined(__alpha__)
#   define DRS_LITTLE_ENDIAN
#endif
#if defined(__i386) || defined(__i386__)
#   define DRS_LITTLE_ENDIAN
#endif
#if defined(__ia64__) || defined(__x86_64__)
#   define DRS_LITTLE_ENDIAN
#endif
#if defined(__arm__)
#   define DRS_LITTLE_ENDIAN
#endif
#if defined(__FreeBSD__) || defined(__OpenBSD__)
#   define DRS_LITTLE_ENDIAN
#endif
#if defined(_WIN32) || defined(_WIN64)
#   define DRS_LITTLE_ENDIAN
#endif

// note: do not give calculation as argument! Otherwise it will be done twice.
#define DRS_NCHAR(x)  ((x) > 0 ? static_cast<char>((x) + .5)  : -static_cast<char>(.5 - (x)))
#define DRS_NSHORT(x) ((x) > 0 ? static_cast<short>((x) + .5) : -static_cast<short>(.5 - (x)))
#define DRS_NINT(x)   ((x) > 0 ? static_cast<int>((x) + .5)   : -static_cast<int>(.5 - (x)))

// header produced by front-end for DRS waveform

// header type
#define DRSH_FLAG_CHANNELV_HEADER  (0<<0)
#define DRSH_FLAG_CHANNELT_HEADER  (1<<0)
#define DRSH_FLAG_CHANNELV_HEADER2 (2<<0) /* channel header with temperature. Used from RUN2019 */
#define DRSH_FLAG_NOT_USED1        (3<<0)

// DRS version
#define DRSH_VERSION4             (1<<3) /* DRS4. Used from RUN2015 to RUN2018 */
#define DRSH_VERSION4_2           (0<<3) /* DRS4 with address of 5 bits for slot. Used from RUN2019 */
#define DRSH_VERSION5             (2<<3)
#define DRSH_VERSION6             (3<<3)

// encoding mode
#define DRSH_ENC_MODE00           (0<<5)
#define DRSH_ENC_MODE01           (1<<5)
#define DRSH_ENC_MODE02           (2<<5)
#define DRSH_ENC_MODE03           (3<<5)
#define DRSH_ENC_MODE04           (4<<5)
#define DRSH_ENC_MODE05           (5<<5)
#define DRSH_ENC_MODE06           (6<<5)
#define DRSH_ENC_MODE07           (7<<5)
#define DRSH_ENC_MODE08           (8<<5)
#define DRSH_ENC_MODE09           (9<<5)
#define DRSH_ENC_MODE10          (10<<5)
#define DRSH_ENC_MODE11          (11<<5)
#define DRSH_ENC_MODE12          (12<<5)
#define DRSH_ENC_MODE13          (13<<5)
#define DRSH_ENC_MODE14          (14<<5)
#define DRSH_ENC_MODE15          (15<<5)

#ifdef __cplusplus
extern "C" {
namespace DRS_ENCODE {
#endif

// header for each DRS waveform
   void byte_swap_drs_header(void* header);

   typedef struct {
      unsigned short flags;       // 0-2: header type, 3-4: drs version, 5-9: encoding mode, 10-15: spare
      unsigned short address;     // DRS4  : 0-3: drs chn,  4: drs chip, 5-9: slot, 10-15: crate
      unsigned short data_size;   // size of waveform in bytes
      unsigned short first_bin;   // first bin which gets encoded, can be used to define ROI
   } DRS_CHANNELV_HEADER;

   typedef struct {
      unsigned short flags;       // 0-2: header type, 3-4: drs version, 5-9: encoding mode, 10-15: spare
      unsigned short address;     // DRS4  : 0-3: drs chn,  4: drs chip, 5-9: slot, 10-15: crate
      unsigned short data_size;   // size of waveform in bytes
      unsigned short first_bin;   // first bin which gets encoded, can be used to define ROI
      short          temperature; // temperature of the WD board in a unit of 0.01 degC
   } DRS_CHANNELV_HEADER2;

   __inline__ unsigned short drs_encmode_to_encflag(unsigned char encmode)
   {
      return ((unsigned short)encmode) << 5;
   }
   __inline__ unsigned char drs_encflag_to_encmode(unsigned short encflag)
   {
      return (unsigned char)((encflag & 0x03E0) >> 5);
   }

   __inline__ unsigned short drs_channelv_extract_header_type(unsigned short flags)   { return flags & 0x0007; }
   __inline__ unsigned short drs_channelv_extract_drs_version(unsigned short flags)   { return flags & 0x0018; }
   __inline__ unsigned short drs_channelv_extract_encoding_mode(unsigned short flags) { return flags & 0x03E0; }
   __inline__ unsigned char drs_channelv_extract_channel(unsigned short address, unsigned short /* version */) {
      return (address & 0x000F) >>  0;
   }
   __inline__ unsigned char drs_channelv_extract_chip(unsigned short address, unsigned short /* version */) {
      return (address & 0x0010) >>  4;
   }
   __inline__ unsigned char drs_channelv_extract_slot(unsigned short address, unsigned short  version) {
      if (version == DRSH_VERSION4) {
         return (address & 0x01E0) >>  5;
      } else {
         return (address & 0x03E0) >>  5;
      }
   }
   __inline__ unsigned char drs_channelv_extract_crate(unsigned short address, unsigned short version) {
      if (version == DRSH_VERSION4) {
         return (address & 0xFE00) >>  9;
      } else {
         return (address & 0xFC00) >> 10;
      }
   }
   const char* drs_channelv_address_string(unsigned short address, unsigned short flags);

   __inline__ void drs_channelv_insert_header_type(unsigned short *flags, unsigned short header)
   {
      *flags = (*flags & (~0x0007)) | ( header & 0x0007);
   }

   __inline__ void drs_channelv_insert_drs_version(unsigned short *flags, unsigned short version)
   {
      *flags = (*flags & (~0x0018)) | (version & 0x0018);
   }

   __inline__ void drs_channelv_insert_encoding_mode(unsigned short *flags, unsigned short encode)
   {
      *flags = (*flags & (~0x03E0)) | (encode  & 0x03E0);
   }

   __inline__ void drs_channelv_insert_channel(unsigned short *address, unsigned char ch, unsigned short /* version */)
   {
      *address = (*address & (~0x000F)) | ((((unsigned short) ch)    & 0x000F) <<  0 );
   }

   __inline__ void drs_channelv_insert_chip(unsigned short *address, unsigned char chip, unsigned short /* version */)
   {
      *address = (*address & (~0x0010)) | ((((unsigned short) chip)  & 0x0001) <<  4 );
   }

   __inline__ void drs_channelv_insert_slot(unsigned short *address, unsigned char slot, unsigned short version)
   {
      if (version == DRSH_VERSION4) {
         *address = (*address & (~0x01E0)) | ((((unsigned short) slot)  & 0x000F) <<  5 );
      } else {
         *address = (*address & (~0x03E0)) | ((((unsigned short) slot)  & 0x001F) <<  5 );
      }
   }

   __inline__ void drs_channelv_insert_crate(unsigned short *address, unsigned char crate, unsigned short version)
   {
      if (version == DRSH_VERSION4) {
         *address = (*address & (~0xFE00)) | ((((unsigned short) crate) & 0x007F) <<  9 );
      } else {
         *address = (*address & (~0xFC00)) | ((((unsigned short) crate) & 0x003F) << 10 );
      }
   }

   // time header for each channel
   typedef struct {
      unsigned short flags;       // 0-2: header type, 3-4: drs version, 5-9: encoding mode, 10-15: spare
      unsigned short address;     // DRS4   : 0-3: drs chn,   4: drs chip, 5-9: slot, 10-15: crate
      unsigned short data_size;   // size of waveform in bytes
      unsigned short first_bin;   // first bin which gets encoded, can be used to define ROI
      unsigned short trigger_cell;// bin in which trigger occured
      unsigned short dt;          // nominal bin width in ps
   } DRS_CHANNELT_HEADER;

   __inline__ unsigned short drs_channelt_extract_header_type(unsigned short flags)   { return flags & 0x0007; }
   __inline__ unsigned short drs_channelt_extract_drs_version(unsigned short flags)   { return flags & 0x0018; }
   __inline__ unsigned short drs_channelt_extract_encoding_mode(unsigned short flags) { return flags & 0x03E0; }
   __inline__ unsigned char drs_channelt_extract_channel(unsigned short address, unsigned short /* version */) {
      return (address & 0x000F) >>  0;
   }
   __inline__ unsigned char drs_channelt_extract_chip(unsigned short address, unsigned short /* version */) {
      return (address & 0x0010) >>  4;
   }
   __inline__ unsigned char drs_channelt_extract_slot(unsigned short address, unsigned short version) 
   {
      if (version == DRSH_VERSION4) {
         return (address & 0x01E0) >>  5;
      } else {
         return (address & 0x03E0) >>  5;
      }
   }
   __inline__ unsigned char drs_channelt_extract_crate(unsigned short address, unsigned short version) {
      if (version == DRSH_VERSION4) {
         return (address & 0xFE00) >>  9;
      } else {
         return (address & 0xFC00) >> 10;
      }
   }
   const char* drs_channelt_address_string(unsigned short address, unsigned short version);

   __inline__ void drs_channelt_insert_header_type(unsigned short *flags, unsigned short header)
   {
      *flags = (*flags & (~0x0007)) | (header  & 0x0007);
   }

   __inline__ void drs_channelt_insert_drs_version(unsigned short *flags, unsigned short version)
   {
      *flags = (*flags & (~0x0018)) | (version & 0x0018);
   }

   __inline__ void drs_channelt_insert_encoding_mode(unsigned short *flags, unsigned short encode)
   {
      *flags = (*flags & (~0x03E0)) | (encode  & 0x03E0);
   }

   __inline__ void drs_channelt_insert_channel(unsigned short *address, unsigned char ch, unsigned short /* version */)
   {
      *address = (*address & (~0x000F)) | ((((unsigned short) ch)    & 0x000F) <<  0 );
   }

   __inline__ void drs_channelt_insert_chip(unsigned short *address, unsigned char chip, unsigned short /* version */)
   {
      *address = (*address & (~0x0010)) | ((((unsigned short) chip)  & 0x0001) <<  4 );
   }

   __inline__ void drs_channelt_insert_slot(unsigned short *address, unsigned char slot, unsigned short version)
   {
      if (version == DRSH_VERSION4) {
         *address = (*address & (~0x01E0)) | ((((unsigned short) slot)  & 0x000F) <<  5 );
      } else {
         *address = (*address & (~0x03E0)) | ((((unsigned short) slot)  & 0x001F) <<  5 );
      }
   }

   __inline__ void drs_channelt_insert_crate(unsigned short *address, unsigned char crate, unsigned short version)
   {
      if (version == DRSH_VERSION4) {
         *address = (*address & (~0xFE00)) | ((((unsigned short) crate) & 0x007F) <<  9 );
      } else {
         *address = (*address & (~0xFC00)) | ((((unsigned short) crate) & 0x003F) << 10 );
      }
   }


   inline int drs_versionbit_to_version(unsigned int verbit)
   {
      switch (verbit) {
      case DRSH_VERSION4:
         return 4;
      case DRSH_VERSION4_2:
         return 4;
      case DRSH_VERSION5:
         return 5;
      case DRSH_VERSION6:
         return 6;
      default:
         return 0;
      }
   }

   inline unsigned short drs_version_to_versionbit(int ver)
   {
      switch (ver) {
      case 4:
         // return DRSH_VERSION4;
         return DRSH_VERSION4_2;
      case 5:
         return DRSH_VERSION5;
      case 6:
         return DRSH_VERSION6;
      default:
         return 0;
      }
   }

   // prototypes
   bool CheckDRSHeaderSize();
   void drs_compose_channelv_header(void *pdata, unsigned short flags, unsigned char crate, unsigned char slot,
                                    unsigned char drs_chip, unsigned char drs_channel,  unsigned short data_size,
                                    unsigned short first_bin);
   void drs_compose_channelv_header2(void *pdata, unsigned short flags, unsigned char crate, unsigned char slot,
                                     unsigned char drs_chip, unsigned char drs_channel,  unsigned short data_size,
                                     unsigned short first_bin, float temperature);
   void drs_compose_channelt_header(void *pdata, unsigned short flags, unsigned char crate, unsigned char slot,
                                    unsigned char drs_chip, unsigned char drs_channel,
                                    unsigned short data_size, unsigned short first_bin,
                                    unsigned short trigger_cell, unsigned short dt);

   // voltage modes
   int drs_encode_voltage_mode00(const unsigned int nbin, const unsigned short first_bin,
                                 const double *__restrict__ v, void *__restrict__ pdata);
   int drs_encode_voltage_mode11(const unsigned int nbin, const unsigned short first_bin,
                                 const double *__restrict__ v,
                                 void *__restrict__ pdata, const int method, const short* par,
                                 const double * __restrict__ t = 0);

   int drs_encode_voltage_mode00s(const unsigned int nbin, const unsigned short first_bin,
                                  const short *__restrict__ v, void *__restrict__ pdata);
   int drs_encode_voltage_mode11s(const unsigned int nbin, const unsigned short first_bin,
                                  const short *__restrict__ v,
                                  void *__restrict__ pdata, const int method, const short* par,
                                  const double * __restrict__ t = 0);

   int drs_encode_voltage_mode00f(const unsigned int nbin, const unsigned short first_bin,
                                  const float *__restrict__ v, void *__restrict__ pdata);
   int drs_encode_voltage_mode11f(const unsigned int nbin, const unsigned short first_bin,
                                  const float *__restrict__ v,
                                  void *__restrict__ pdata, const int method, const short* par,
                                  const double * __restrict__ t = 0);

   int drs_decode_voltage_mode00(const int dsize, const unsigned short first_bin,
                                 const void *__restrict__ pdata, double *__restrict__ v);
   int drs_decode_voltage_mode11(const int dsize, const unsigned short first_bin,
                                 const void *__restrict__ pdata, double *__restrict__ v,
                                 int *__restrict__ rebinSizeArray);

   // time modes
   int drs_encode_time_mode00(const unsigned int nbin, const unsigned short dt,
                              const unsigned short first_bin, const double *__restrict__ t,
                              void *__restrict__ pdata);
   int drs_encode_time_mode08(const unsigned int nbin, const unsigned short first_bin,
                              const double *__restrict__ t, void *__restrict__ pdata);
   int drs_encode_time_mode09(const unsigned int nbin, const unsigned short first_bin,
                              const double *__restrict__ t, void *__restrict__ pdata);

   int drs_encode_time_mode00f(const unsigned int nbin, const unsigned short dt,
                               const unsigned short first_bin, const float *__restrict__ t,
                               void *__restrict__ pdata);
   int drs_encode_time_mode08f(const unsigned int nbin, const unsigned short first_bin,
                               const float *__restrict__ t, void *__restrict__ pdata);
   int drs_encode_time_mode09f(const unsigned int nbin, const unsigned short first_bin,
                               const float *__restrict__ t, void *__restrict__ pdata);

   int drs_decode_time_mode00(const int dsize, const unsigned short first_bin, const unsigned short dt,
                              const void *__restrict__ pdata, double *__restrict__ t);
   int drs_decode_time_mode08(const int dsize, const unsigned short first_bin,
                              const void *__restrict__ pdata, double *__restrict__ t);
   int drs_decode_time_mode09(const int dsize, const unsigned short first_bin,
                              const void *__restrict__ pdata, double *__restrict__ t);

   // maximum data size after encoding
   __inline__ int DRS_MAX_ENCODE_VOLTAGE_SIZE_MODE00(int i)
   {
      return (int) (2 * i);
   }

   __inline__ int DRS_MAX_ENCODE_VOLTAGE_SIZE_MODE11(int i)
   {
      return (int) (4 * i);
   }

   __inline__ int DRS_MAX_ENCODE_TIME_SIZE_MODE00(int i)
   {
      return (int) (i + 4);
   }

   __inline__ int DRS_MAX_ENCODE_TIME_SIZE_MODE08(int i)
   {
      return (int) (8 * i);
   }

   __inline__ int DRS_MAX_ENCODE_TIME_SIZE_MODE09(int i)
   {
      return (int) (4 * i);
   }

   // merge time and voltage data for rebin decoding
   int decode_rebin(int *rebinSize, double *timeSrc, double *timeDst,
                    double *ampSrc = 0, double *ampDst = 0);


   // Byte and Word swapping big endian <-> little endian
   __inline__ void drs_word_swap(void *x) {
      unsigned char tmp;
      tmp = *(static_cast<unsigned char *>(x) + 0);
      *(static_cast<unsigned char*>(x) + 0) = *(static_cast<unsigned char*>(x) + 1);
      *(static_cast<unsigned char*>(x) + 1) = tmp;
   }

   __inline__ void drs_dword_swap(void *x) {
      unsigned char tmp;
      tmp= *(static_cast<unsigned char*>(x) + 0);
      *(static_cast<unsigned char*>(x) + 0) = *(static_cast<unsigned char*>(x) + 3);
      *(static_cast<unsigned char*>(x) + 3) = tmp;
      tmp= *(static_cast<unsigned char*>(x) + 1);
      *(static_cast<unsigned char*>(x) + 1) = *(static_cast<unsigned char*>(x) + 2);
      *(static_cast<unsigned char*>(x) + 2) = tmp;
   }

   __inline__ void drs_qword_swap(void *x) {
      unsigned char tmp;
      tmp= *(static_cast<unsigned char*>(x) + 0);
      *(static_cast<unsigned char*>(x) + 0) = *(static_cast<unsigned char*>(x) + 7);
      *(static_cast<unsigned char*>(x) + 7) = tmp;
      tmp= *(static_cast<unsigned char*>(x) + 1);
      *(static_cast<unsigned char*>(x) + 1) = *(static_cast<unsigned char*>(x) + 6);
      *(static_cast<unsigned char*>(x) + 6) = tmp;
      tmp= *(static_cast<unsigned char*>(x) + 2);
      *(static_cast<unsigned char*>(x) + 2) = *(static_cast<unsigned char*>(x) + 5);
      *(static_cast<unsigned char*>(x) + 5) = tmp;
      tmp= *(static_cast<unsigned char*>(x) + 3);
      *(static_cast<unsigned char*>(x) + 3) = *(static_cast<unsigned char*>(x) + 4);
      *(static_cast<unsigned char*>(x) + 4) = tmp;
   }

#ifdef __cplusplus
};
using namespace DRS_ENCODE;
}                               // extern "C"

#endif

#endif
