#ifndef DRSCONST_H
#define DRSCONST_H

//#include <Rtypes.h>
//#include <TMath.h>
//#include "units/MEGSystemOfUnits.h"
//#include "drs_encode/drs_encode.h"

constexpr int    kDRSBins                       = 1024;
constexpr int    kNumberOfChannelsOnDRSChip     = 9;
constexpr int    kNumberOfChannelsOnDRSChipVer4 = 9;
//constexpr Double_t kDRSInputImpedance             = 50. * ohm; // input impedance

#endif
