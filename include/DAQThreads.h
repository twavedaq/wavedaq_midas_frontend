#include "DAQLib.h"
#include "WDLib.h"
#include "drs_encode/drs_encode.h"
#include "mfe.h"
#include "msystem.h"

#ifndef DAQThreads
#define DAQThreads

//Error definition
//#define WDAQFE_ERROR_MISSINGCALIB WDAQLIB_ERROR_LAST+1

const int MidasZeroSuppressionNRebinThr = 2;

typedef struct {
   unsigned short Prescaling;
   unsigned short ClkROIStart;
   unsigned short ClkROIEnd;
   unsigned short SignalROIStart;
   unsigned short SignalROIEnd;
   unsigned short RegionStart;
   unsigned short RegionEnd;
   unsigned short RebinFactor;
   float RebinSignalThr;
   float WfmSuppressionThr;
   float RebinThr[MidasZeroSuppressionNRebinThr];
   unsigned short AdcROIStart;
   unsigned short AdcROIEnd;
} MidasZeroSuppressionConfig;

//Custom thread for Midas bank formatting
class MidasProducer: public DAQThread {
private:
   //shared members with access mutex
   static std::mutex fSharedMutex;
   static DWORD fScalerTime;
   EQUIPMENT* fEquipment;

   //interal members
   int fMidasThreadIndex;
   unsigned long fDroppedEvents;
   INT fRbh;
   DAQBuffer<WDAQEvent> *fSource;
   std::chrono::microseconds fBufferWaitDuration;
   WDSystem *fSystem;

   std::map<unsigned int, MidasZeroSuppressionConfig> fSwZeroSuppressionConfig;
   std::mutex fSwZeroSuppressionMutex;

   //Implemented from DAQThread
   void Setup();
   void Begin();
   void Loop();
   void End();

   //actual readout function
   void Readout(WDAQEvent* event_ptr, WORD* pevent);
   void ReadoutScaler(WDAQEvent* event_ptr, WORD* pevent);

public:
   MidasProducer(DAQBuffer<WDAQEvent> *source, WDSystem* system, EQUIPMENT* equipment, int index, DAQSystem* parent = nullptr);

   //public setter of waittime getting Midas buffer
   void SetBufferWaitDuration(std::chrono::microseconds d){
      fBufferWaitDuration = d;
   }

   unsigned long GetDroppedEvents() const { return fDroppedEvents; }

   //Set zero suppression parameter
   void SetSwZeroSuppression(unsigned int board, unsigned short prescaling, unsigned short clkROIstart, unsigned short clkROIend, unsigned short signalROIstart, unsigned short signalROIend, unsigned short regionStart, unsigned short regionEnd, unsigned short rebinFactor, float rebinSignalThr, float wfmSuppressionThr, float* rebinThr, unsigned short adcROIstart, unsigned short adcROIend);
   void SetSwZeroSuppressionPrescaling(unsigned int board, unsigned short parameter);
   void SetSwZeroSuppressionClkROIStart(unsigned int board, unsigned short parameter);
   void SetSwZeroSuppressionClkROIEnd(unsigned int board, unsigned short parameter);
   void SetSwZeroSuppressionSignalROIStart(unsigned int board, unsigned short parameter);
   void SetSwZeroSuppressionSignalROIEnd(unsigned int board, unsigned short parameter);
   void SetSwZeroSuppressionRegionStart(unsigned int board, unsigned short parameter);
   void SetSwZeroSuppressionRegionEnd(unsigned int board, unsigned short parameter);
   void SetSwZeroSuppressionRebinFactor(unsigned int board, unsigned short parameter);
   void SetSwZeroSuppressionRebinSignalThr(unsigned int board, float parameter);
   void SetSwZeroSuppressionWfmSuppressionThr(unsigned int board, float parameter);
   void SetSwZeroSuppressionRebinThr(unsigned int board, float* parameters);
   void SetSwZeroSuppressionAdcROIStart(unsigned int board, unsigned short parameter);
   void SetSwZeroSuppressionAdcROIEnd(unsigned int board, unsigned short parameter);

}; 
#endif //DAQThreads
