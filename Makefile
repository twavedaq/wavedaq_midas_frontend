#####################################################################
#
#  Name:         Makefile
#  Created by:   Stefan Ritt
#
#  Contents:     Makefile for MEG II WaveDREAM frontend
#
#####################################################################

# directories
MXML_DIR      = $(MIDASSYS)/mxml/
MIDASINC      = $(MIDASSYS)/include
MSCB_DIR      = $(WDBSYS)/software/mscb
WDB_DIR       = $(WDBSYS)/software
DCB_DIR       = $(WDBSYS)/software
TCB_DIR       = $(WDBSYS)/software
WDAQ_DIR      = $(WDBSYS)/software

# compiler, flags and libs
CC            = cc
CPP           = g++
FLAGS         = -g -DOS_LINUX -Wall -O3
FLAGS         += -std=c++11

ifeq ($(shell uname),Darwin)
FLAGS         += -DOS_DARWIN -DHAVE_STRLCPY 
LIBS          =  -framework IOKit -framework CoreFoundation
else
FLAGS         += 
LIBS          =  -lrt -lbsd
endif

ifdef MIDAS_MAX_EVENT_SIZE
FLAGS         += -DMAX_EVENT_SIZE=$(MIDAS_MAX_EVENT_SIZE)
endif
FLAGS += -D WD2_DONT_INCLUDE_REG_ACCESS_VARS
FLAGS += -D DCB_DONT_INCLUDE_REG_ACCESS_VARS
FLAGS         += -I$(MIDASINC) -I$(MXML_DIR) -I$(MSCB_DIR)/include -Iinclude -I$(WDB_DIR)/include -I$(TCB_DIR)/include -I$(DCB_DIR)/include -I$(DCB_DIR)/dcb/app/src -I$(WDAQ_DIR)/include
LIBS          += -lm -lutil -lpthread

# system objects
MIDAS_OBJ     = midas.o midas_cxx.o elog.o alarm.o mfe.o system.o odb.o mrpc.o
MXML_OBJ      = mxml.o strlcpy.o
MSCB_OBJ      = mscb.o mscbrpc.o

#-------------------------------------------------------------------
# Drivers needed by the frontend program
#                 
WDAQ_OBJ      = WDLib.o WDAQLib.o
DCB_OBJ       = DCBLib.o
TCB_OBJ       = TCBLib.o
WDB_OBJ       = WDBLib.o averager.o
DCB_OBJ       = DCBLib.o
ENC_CXX_OBJ   = drs_encode.o

DRV_OBJ       = $(WDAQ_OBJ) $(TCB_OBJ) $(WDB_OBJ) $(DCB_OBJ)

OBJECTS       = wdaq_fe.o $(MIDAS_OBJ) $(MXML_OBJ) $(MSCB_OBJ) $(DRV_OBJ) $(ENC_CXX_OBJ)
OUTNAME       = wdaq_fe

all: $(OUTNAME)

$(OUTNAME): $(OBJECTS)
	$(CPP) $(FLAGS) $(OBJECTS) -o $(OUTNAME) $(LIBS)

$(TCB_OBJ): %.o: $(TCB_DIR)/src/%.cpp
	$(CPP) $(FLAGS) -o $@ -c $<

$(WDAQ_OBJ): %.o: $(WDAQ_DIR)/src/%.cpp
	$(CPP) $(FLAGS) -o $@ -c $<

$(WDB_OBJ): %.o: $(WDB_DIR)/src/%.cpp
	$(CPP) $(FLAGS) -o $@ -c $<

$(DCB_OBJ): %.o: $(DCB_DIR)/src/%.cpp
	$(CPP) $(FLAGS) -o $@ -c $<

wdaq_fe.o: src/wdaq_fe.cpp
	$(CPP) $(FLAGS) -o $@ -c $<

$(MSCB_OBJ): %.o: $(MSCB_DIR)/src/%.cxx
	$(CPP) $(FLAGS) -o $@ -c $<

$(MIDAS_OBJ): %.o: $(MIDASSYS)/src/%.cxx $(MIDASINC)/midas.h
	$(CPP) $(FLAGS) -o $@ -c $<

$(MXML_OBJ): %.o: $(MXML_DIR)/%.cxx $(MXML_DIR)/mxml.h
	$(CPP) $(FLAGS) -o $@ -c $<

$(ENC_CXX_OBJ): %.o: src/%.cpp
	$(CPP) $(FLAGS) -o $@ -c $<

clean:
	rm -f *.o *~ \#* $(OBJECTS) $(OUTNAME)
