#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <zlib.h>
#include <string.h>
#include <iostream>

#include "drs_encode/drs_encode.h"
#include "constants/drs/drsconst.h"

// define it to reset before 'first_bin' and after 'first_bin + bin' during decoding.
#undef RESET_WF

namespace DRS_ENCODE {
   const int kMaxChar = 127;
   int drs_divide(int a, const int bhalf, const int bminusone, const int bbit);
   int drs_rebin(const unsigned int nbin, const short *v, void *pdata,
                 const int rebinLevel, const double *t);
   void byte_swap_drs_channelt_header(DRS_CHANNELT_HEADER *header);
   void byte_swap_drs_channelv_header(DRS_CHANNELV_HEADER *header);
   void byte_swap_drs_channelv_header2(DRS_CHANNELV_HEADER2 *header);   

   template <class T>
   int drs_encode_voltage_mode00_template(const unsigned int nbin,
                                          const unsigned short first_bin,
                                          const T *__restrict__ v,
                                          void *__restrict__ pdata);
   template <class T>
   int drs_encode_time_mode00_template(const unsigned int nbin,
                                       const unsigned short dt,
                                       const unsigned short first_bin,
                                       const T *__restrict__ t,
                                       void *__restrict__ pdata);

   __inline__ double drs_mean(int n, const double *a)
   {
      if (n <= 0 || !a) return 0;

      double sum = 0;
      int i;
      for (i = 0; i < n; i++) {
         sum += a[i];
      }

      return sum / n;
   }
}

using namespace std;
using namespace DRS_ENCODE;

//______________________________________________________________________________
bool DRS_ENCODE::CheckDRSHeaderSize() {
   if (sizeof(DRS_CHANNELV_HEADER)  !=  8 ||
       sizeof(DRS_CHANNELT_HEADER)  != 12 ||
       sizeof(DRS_CHANNELV_HEADER2) != 10) {
      cerr<<"DRS header size is not correct, probably due to padding."<<endl<<
            "Please replace compiler or use proper compile option."<<endl;
      return false;
   }
   return true;
}

//______________________________________________________________________________
__inline__ int DRS_ENCODE::drs_divide(int a, const int bhalf, const int bminusone,
                                      const int bbit)
{
   // return nearest interger to "((float) a) / ((float) b)"
   // b must be,         2^?
   // bhalf must be,     bhalf = b / 2
   // bminusone must be, bminusone = b - 1
   // bbit must be,      b = 2^bbit
   bool pos = (a >= 0);

   if (!pos) {
      a = -a;
   }
   int ret = a >> bbit;
   if ((a & bminusone) >= bhalf) {
      ret += 1;
   }
   return pos ? ret : -ret;
}

//______________________________________________________________________________
int DRS_ENCODE::drs_rebin(const unsigned int nbin, const short *__restrict__ v,
                          void *__restrict__ pdata, const int rebinLevel,
                          const double *__restrict__ t)
{
   int dsize;
   int ibin;
   char* pc = static_cast<char*>(pdata);

   if (rebinLevel <= 0) {
      // no rebinning
      dsize = nbin * sizeof(short);
#ifdef DRS_LITTLE_ENDIAN
      memcpy(pc, v, dsize);
#else
      for(ibin = 0; ibin < static_cast<int>(nbin); ibin++) {
         pc[ibin * sizeof(short)]     = v[ibin] & 0x00FF;
         pc[ibin * sizeof(short) + 1] = (v[ibin] & 0xFF00) >> 8;
      }
#endif
      return dsize;
   }

   int groupSize = 1 << rebinLevel;   // = 2^rebinLevel
   int nNewBins = nbin >> rebinLevel; // = nbin / groupSize

   // check if nbin is dividable by groupSize
   if (nbin & (groupSize - 1)) {
      cerr<<"Error from drs_encode_rebin: nbin must be dividable by (1<<rebinLevel)"<<endl;
      return 0;
   }

   int orgbin = 0;
   int jbin;
   short sdata;
   double binContentDouble;
   double binWidthDouble;
   int binContent;
   if (t) { // weighted average
      for (ibin = 0; ibin < nNewBins; ibin++) {
         binContentDouble = 0;
         binWidthDouble   = 0;
         for (jbin = groupSize - 1; jbin >= 0; jbin--) {
            binContentDouble += v[orgbin + jbin] * t[orgbin + jbin];
            binWidthDouble   += t[orgbin + jbin];
         }
         sdata = DRS_NSHORT(binContentDouble / binWidthDouble);
         pc[ibin * sizeof(short)]     = sdata & 0x00FF;
         pc[ibin * sizeof(short) + 1] = (sdata & 0xFF00) >> 8;
         orgbin += groupSize;
      }
   } else { // flat average
      for (ibin = 0; ibin < nNewBins; ibin++) {
         binContent = 0;
         for (jbin = groupSize - 1; jbin >= 0; jbin--) {
            binContent += v[orgbin + jbin];
         }
         sdata = drs_divide(binContent, groupSize >> 1,
                            groupSize - 1, rebinLevel);
         pc[ibin * sizeof(short)]     = sdata & 0x00FF;
         pc[ibin * sizeof(short) + 1] = (sdata & 0xFF00) >> 8;
         orgbin += groupSize;
      }
   }

   return nNewBins * sizeof(short);
}

/*******************************************************************************
 * Compose waveform channelv header
 *
 * Input
 *   flags       0-2: header type, 3-4: drs version, 5-9: encoding mode,
 *               10-15: spare
 *   crate       crate number
 *   slot        location 0..34
 *   drs_chip    Number of DRS chip on board
 *   drs_channel Number of DRS channel 0..7
 *   data_size   Waveform size in bytes
 *   first_bin     Address 0..1023 of first bin in waveform which is encoded
 * Ouput
 *   header at address "pdata"
 *******************************************************************************/

//______________________________________________________________________________
void DRS_ENCODE::drs_compose_channelv_header(void *pdata, unsigned short flags,
                                             unsigned char crate,
                                             unsigned char slot,
                                             unsigned char drs_chip,
                                             unsigned char drs_channel,
                                             unsigned short data_size,
                                             unsigned short first_bin)
{
   DRS_CHANNELV_HEADER *ph = static_cast<DRS_CHANNELV_HEADER*>(pdata);

   ph->flags = flags | DRSH_FLAG_CHANNELV_HEADER;
   unsigned short address = 0;
   unsigned short version = drs_versionbit_to_version(drs_channelv_extract_drs_version(ph->flags));
   drs_channelv_insert_crate(&address, crate, version);
   drs_channelv_insert_slot(&address, slot, version);
   drs_channelv_insert_chip(&address, drs_chip, version);
   drs_channelv_insert_channel(&address, drs_channel, version);
   ph->address = address;
   ph->data_size = data_size;
   ph->first_bin = first_bin;
}

/*******************************************************************************
 * Compose waveform channelv header2
 *
 * Input
 *   flags       0-2: header type, 3-4: drs version, 5-9: encoding mode,
 *               10-15: spare
 *   crate       crate number
 *   slot        location 0..34
 *   drs_chip    Number of DRS chip on board
 *   drs_channel Number of DRS channel 0..7
 *   data_size   Waveform size in bytes
 *   first_bin   Address 0..1023 of first bin in waveform which is encoded
 *   temperature Temperature of the board in degC (float)
 * Ouput
 *   header at address "pdata"
 *******************************************************************************/

//______________________________________________________________________________
void DRS_ENCODE::drs_compose_channelv_header2(void *pdata, unsigned short flags,
                                             unsigned char crate,
                                             unsigned char slot,
                                             unsigned char drs_chip,
                                             unsigned char drs_channel,
                                             unsigned short data_size,
                                             unsigned short first_bin,
                                             float          temperature)
{
   DRS_CHANNELV_HEADER2 *ph = static_cast<DRS_CHANNELV_HEADER2*>(pdata);

   ph->flags = flags | DRSH_FLAG_CHANNELV_HEADER2;
   unsigned short address = 0;
   unsigned short version = drs_versionbit_to_version(drs_channelv_extract_drs_version(ph->flags));
   drs_channelv_insert_crate(&address, crate, version);
   drs_channelv_insert_slot(&address, slot, version);
   drs_channelv_insert_chip(&address, drs_chip, version);
   drs_channelv_insert_channel(&address, drs_channel, version);
   ph->address = address;
   ph->data_size = data_size;
   ph->first_bin = first_bin;
   ph->temperature = static_cast<short>(temperature * 100 + 0.5);
}


/*******************************************************************************
 * Compose waveform channel time header
 *
 * Input
 *   flags         0-2: header fmt, 3-4: drs version, 5-9: encoding mode,
 *                 10-15: spare
 *   crate         crate number
 *   slot          location 0..34
 *   drs_chip      Number of DRS chip on board
 *   drs_channel   Number of DRS channel 0..7
 *   data_size     Waveform size in bytes
 *   first_bin     Address 0..1023 of first bin in waveform which is encoded
 *   trigger_cell  DRS cell 0..1023 where trigger signal occured
 *   dt            time difference between bins given in ps
 * Ouput
 *   header at address "pdata"
 *******************************************************************************/

//______________________________________________________________________________
void DRS_ENCODE::drs_compose_channelt_header(void *pdata, unsigned short flags,
                                             unsigned char crate, unsigned char slot,
                                             unsigned char drs_chip,
                                             unsigned char drs_channel,
                                             unsigned short data_size,
                                             unsigned short first_bin,
                                             unsigned short trigger_cell,
                                             unsigned short dt)
{
   DRS_CHANNELT_HEADER *ph = static_cast<DRS_CHANNELT_HEADER*>(pdata);

   ph->flags = flags | DRSH_FLAG_CHANNELT_HEADER;
   unsigned short address = 0;
   unsigned short version = drs_versionbit_to_version(drs_channelt_extract_drs_version(ph->flags));
   drs_channelt_insert_crate(&address, crate, version);
   drs_channelt_insert_slot(&address, slot, version);
   drs_channelt_insert_chip(&address, drs_chip, version);
   drs_channelt_insert_channel(&address, drs_channel, version);
   ph->address = address;
   ph->data_size = data_size;
   ph->first_bin = first_bin;
   ph->trigger_cell = trigger_cell;
   ph->dt = dt;
}

/*******************************************************************************
 * Byte swap DRS header
 *
 * Input
 *   header      pointer to DRS header
 *******************************************************************************/
//______________________________________________________________________________
void DRS_ENCODE::byte_swap_drs_channelv_header(DRS_CHANNELV_HEADER *header)
{
   drs_word_swap(&(header->flags));
   drs_word_swap(&(header->address));
   drs_word_swap(&(header->data_size));
   drs_word_swap(&(header->first_bin));
   return;
}

//______________________________________________________________________________
void DRS_ENCODE::byte_swap_drs_channelv_header2(DRS_CHANNELV_HEADER2 *header)
{
   drs_word_swap(&(header->flags));
   drs_word_swap(&(header->address));
   drs_word_swap(&(header->data_size));
   drs_word_swap(&(header->first_bin));
   drs_word_swap(&(header->temperature));   
   return;
}

//______________________________________________________________________________
void DRS_ENCODE::byte_swap_drs_channelt_header(DRS_CHANNELT_HEADER *header)
{
   drs_word_swap(&(header->flags));
   drs_word_swap(&(header->address));
   drs_word_swap(&(header->data_size));
   drs_word_swap(&(header->first_bin));
   drs_word_swap(&(header->trigger_cell));
   drs_word_swap(&(header->dt));
   return;
}

//______________________________________________________________________________
void DRS_ENCODE::byte_swap_drs_header(void *header)
{
   unsigned short flags = static_cast<DRS_CHANNELV_HEADER*>(header)->flags;
   drs_word_swap(&flags);
   int header_type = drs_channelv_extract_header_type(flags);
   if (header_type == DRSH_FLAG_CHANNELV_HEADER) {
      byte_swap_drs_channelv_header(static_cast<DRS_CHANNELV_HEADER*>(header));
   } else if (header_type == DRSH_FLAG_CHANNELV_HEADER2) {
      byte_swap_drs_channelv_header2(static_cast<DRS_CHANNELV_HEADER2*>(header));
   } else if (header_type == DRSH_FLAG_CHANNELT_HEADER) {
      byte_swap_drs_channelt_header(static_cast<DRS_CHANNELT_HEADER*>(header));
   } else {
      abort();
   }
   return;
}

//______________________________________________________________________________
const char* DRS_ENCODE::drs_channelv_address_string(unsigned short address, unsigned short version)
{
   static char str[256];
   sprintf(str, "Cr:%d-Sl:%d-Cp:%d-Ch:%d",
           drs_channelv_extract_crate(address, version),
           drs_channelv_extract_slot(address, version),
           drs_channelv_extract_chip(address, version),
           drs_channelv_extract_channel(address, version));
   return str;
}

/*******************************************************************************
 * DRS voltage encoding mode 0
 * no compression
 * voltage is stored in short [x0.1mV]
 *******************************************************************************/
const double DRS_CF_V_MODE00 = 1e4;     // conversion factor for voltage

//______________________________________________________________________________
template <class T>
int DRS_ENCODE::drs_encode_voltage_mode00_template(const unsigned int nbin,
                                                   const unsigned short first_bin,
                                                   const T *__restrict__ v,
                                                   void *__restrict__ pdata)
{
/*
Inputs
  nbin   : number of bins of DRS
  v      : array of voltage
Outputs
  pdata  : encoded data
Return   : number of byte of encoded data
*/
   unsigned int ibin;
   short sdata;
   int idata = 0;
   const T *pv = v + first_bin;

#if defined(VLA_AVAILABLE)
   double encoded[nbin];
#else
   double *encoded;
#endif

   if (nbin == 0 || v == 0 || pdata == 0) {
      return 0;
   }

#if !defined(VLA_AVAILABLE)
   encoded = new double[nbin];
#endif

   char *pc = static_cast<char*>(pdata);

   // this loop can be vectorized
   for (ibin = 0; ibin < nbin; ibin++) {
      encoded[ibin] = pv[ibin] * DRS_CF_V_MODE00;
   }
   for (ibin = 0; ibin < nbin; ibin++) {
      sdata = DRS_NSHORT(encoded[ibin]);
      pc[idata++] = sdata & 0x00FF;
      pc[idata++] = (sdata & 0xFF00) >> 8;
   }

#if !defined(VLA_AVAILABLE)
   delete [] encoded;
#endif

   return idata;
}

//______________________________________________________________________________
int DRS_ENCODE::drs_decode_voltage_mode00(const int dsize,
                                          const unsigned short first_bin,
                                          const void *__restrict__ pdata,
                                          double *__restrict__ v)
{
/*
Inputs
  dsize  : number of byte of encoded data
  pdata  : encoded data
Outputs
  v      : array of voltage
Return   : number of bins of DRS
*/
   const unsigned int nbin = static_cast<int>(dsize / sizeof(short));
   const unsigned int last_bin = first_bin + nbin;
   unsigned int ibin;
   const short *ps = static_cast<const short*>(pdata);
#ifndef DRS_LITTLE_ENDIAN
   short sdata;
#endif

   if (v == 0) {
      return 0;
   }

   if (dsize <= 0 || pdata == 0) {
#ifdef RESET_WF
      memset(v, 0, kDRSBins * sizeof(double));
#endif
      return 0;
   }

   for (ibin = first_bin; ibin < last_bin; ibin++) {
#ifdef DRS_LITTLE_ENDIAN
      v[ibin] = (*ps) * (1. / DRS_CF_V_MODE00);
#else
      sdata = *ps;
      drs_word_swap(&sdata);
      v[ibin] = sdata * (1. / DRS_CF_V_MODE00);
#endif
      ps++;
   }

#ifdef RESET_WF
   memset(v, 0, first_bin * sizeof(double));
   memset(v + first_bin + nbin, 0, (kDRSBins - (first_bin + nbin)) * sizeof(double));
#endif

   return nbin;
}

//______________________________________________________________________________
int DRS_ENCODE::drs_encode_voltage_mode00(const unsigned int nbin,
                                          const unsigned short first_bin,
                                          const double *__restrict__ v,
                                          void *__restrict__ pdata)
{
   return drs_encode_voltage_mode00_template(nbin, first_bin, v, pdata);
}

//______________________________________________________________________________
int DRS_ENCODE::drs_encode_voltage_mode00f(const unsigned int nbin,
                                           const unsigned short first_bin,
                                           const float *__restrict__ v,
                                           void *__restrict__ pdata)
{
   return drs_encode_voltage_mode00_template(nbin, first_bin, v, pdata);
}

//______________________________________________________________________________
int DRS_ENCODE::drs_encode_voltage_mode00s(const unsigned int nbin,
                                           const unsigned short first_bin,
                                           const short *__restrict__ v,
                                           void *__restrict__ pdata)
{
/*
Inputs
  nbin   : number of bins of DRS
  v      : array of voltage
Outputs
  pdata  : encoded data
Return   : number of byte of encoded data
*/
   const int dsize =  sizeof(short) * nbin;
#ifdef DRS_LITTLE_ENDIAN
   memcpy(pdata, v + first_bin, dsize);
#else
   char *pc = static_cast<char*>(pdata);
   for(unsigned int ibin = first_bin; ibin < nbin; ibin++) {
      pc[ibin * sizeof(short)]     = v[ibin] & 0x00FF;
      pc[ibin * sizeof(short) + 1] = (v[ibin] & 0xFF00) >> 8;
   }
#endif
   return dsize;
}


/*******************************************************************************
 * DRS voltage encoding mode 11
 * Rebinning encode.
 * voltage is stored in short [x0.1mV]
 *******************************************************************************/
const double DRS_CF_V_MODE11 = 1e4;     // conversion factor for voltage
const int kMode11RebinChangeMask = 0xFF00;
const int kMode11RebinChangeFlag = 0x8000;

//______________________________________________________________________________
int DRS_ENCODE::drs_encode_voltage_mode11s(const unsigned int nbin,
                                           const unsigned short first_bin,
                                           const short *__restrict__ v,
                                           void *__restrict__ pdata,
                                           const int method, const short* par,
                                           const double * __restrict__ t)
{
/*
Inputs
  nbin         : number of bins of DRS
  v            : array of voltage
  rebin_method : method of re-binning
  par          : parameters for rebinning
  t            : array of time width corresponding to voltage bins.
  t              If the pointer is 0, this parameter is not used
Outputs
  pdata        : encoded data
Return         : number of byte of encoded data

Method-0
 Rebin with the same level for all bins.
 par[0] : rebin level. 2^par[0] bins are averaged.
          (ex. 0->1bin, 1->2bin, 2->4bin....)

Method-1
 Rebin with a level for several time regions.
 par[0] : rebin level. 2^par[0] bins are averaged.
          (ex. 0->1bin, 1->2bin, 2->4bin....)
 par[1] : number of time regions
 par[2] : start bin of the first region
 par[3] : end bin of the first region
 par[4] : start bin of the second region
 par[5] : end bin of the second region
 .
 .
 .
*/
   int nRegion;
   int iRegion, jRegion;
   int dsize;
   int *regionStart;
   int *regionEnd;
   int *regionRebinLevel;
   int idata;

   if (!nbin) {
      return 0;
   }

   char *pc = static_cast<char*>(pdata);

   if (method == 0) {
      dsize = 0;
      if (par[0] > 0) {
         pc[0] = static_cast<char>(par[0]);
         pc[1] = static_cast<unsigned char>(kMode11RebinChangeFlag >> 8);
         dsize += sizeof(short);
      }
      dsize += drs_rebin(nbin, v + first_bin, pc + dsize, par[0], t);
   } else if (method == 1) {
      // assuming
      // * start and end bins are aligned to (2^rebinLevel)
      // * no overwrap of regions
      regionStart = new int[par[1] * 2 + 1];
      regionEnd = new int[par[1] * 2 + 1];
      regionRebinLevel = new int[par[1] * 2 + 1];
      iRegion = 0;
      if (par[2] != first_bin) {
         regionStart[iRegion] = first_bin;
         regionEnd[iRegion] = par[2];
         regionRebinLevel[iRegion] = 0;
         iRegion++;
      }
      for (jRegion = 0; jRegion < par[1] - 1; jRegion++) {
         regionStart[iRegion] = par[2 * jRegion + 2];
         regionEnd[iRegion] = par[2 * jRegion + 2 + 1];
         regionRebinLevel[iRegion] = par[0];
         iRegion++;

         regionStart[iRegion] = par[2 * jRegion + 2 + 1];
         regionEnd[iRegion] = par[2 * (jRegion + 1) + 2];
         regionRebinLevel[iRegion] = 0;
         iRegion++;
      }
      regionStart[iRegion] = par[2 * jRegion + 2];
      regionEnd[iRegion] = par[2 * jRegion + 2 + 1];
      regionRebinLevel[iRegion] = par[0];
      iRegion++;
      if (par[2 * jRegion + 2 + 1] != static_cast<int>(first_bin + nbin)) {
         regionStart[iRegion] = par[2 * jRegion + 2 + 1];
         regionEnd[iRegion] = first_bin + nbin;
         regionRebinLevel[iRegion] = 0;
         iRegion++;
      }
      nRegion = iRegion;

      idata = 0;
      for (iRegion = 0; iRegion < nRegion; iRegion++) {
         if (iRegion != 0 || regionRebinLevel[iRegion] != 0) {
            pc[idata++] = regionRebinLevel[iRegion];
            pc[idata++] = static_cast<char>(kMode11RebinChangeFlag >> 8);
         }
         idata += drs_rebin(regionEnd[iRegion] - regionStart[iRegion],
                            v + regionStart[iRegion],
                            pc + idata,
                            regionRebinLevel[iRegion], t ? t +
                            regionStart[iRegion] : 0);
      }
      dsize = idata;
      delete [] regionStart;
      delete [] regionEnd;
      delete [] regionRebinLevel;
   } else { // unknown method
      dsize = sizeof(short) * nbin;
#ifdef DRS_LITTLE_ENDIAN
      memcpy(pc, v, dsize);
#else
      for(unsigned ibin = 0; ibin < nbin; ibin++) {
         pc[ibin * sizeof(short)]     = v[ibin] & 0x00FF;
         pc[ibin * sizeof(short) + 1] = (v[ibin] & 0xFF00) >> 8;
      }
#endif
   }

   return dsize;
}

//______________________________________________________________________________
int DRS_ENCODE::drs_encode_voltage_mode11(const unsigned int nbin,
                                          const unsigned short first_bin,
                                          const double *__restrict__ v,
                                          void *__restrict__ pdata,
                                          const int method,
                                          const short* par,
                                          const double * __restrict__ t)
{
   if (!nbin || !v) {
      return 0;
   }

   short *shorttmp = new short[kDRSBins];
   int i;
   for (i = first_bin + nbin - 1; i >= static_cast<int>(first_bin); i--) {
      shorttmp[i] = DRS_NSHORT(v[i] * DRS_CF_V_MODE11);
   }
   int dsize =  drs_encode_voltage_mode11s(nbin, first_bin, shorttmp, pdata,
                                           method, par, t);
   delete [] shorttmp;
   return dsize;
}

//______________________________________________________________________________
int DRS_ENCODE::drs_encode_voltage_mode11f(const unsigned int nbin,
                                           const unsigned short first_bin,
                                           const float *__restrict__ v,
                                           void *__restrict__ pdata,
                                           const int method,
                                           const short* par,
                                           const double * __restrict__ t)
{
   if (!nbin || !v) {
      return 0;
   }

   short *shorttmp = new short[kDRSBins];
   int i;
   for (i = first_bin + nbin - 1; i >= static_cast<int>(first_bin); i--) {
      shorttmp[i] = DRS_NSHORT(v[i] * DRS_CF_V_MODE11);
   }
   int dsize =  drs_encode_voltage_mode11s(nbin, first_bin, shorttmp, pdata,
                                           method, par, t);
   delete [] shorttmp;
   return dsize;
}

//______________________________________________________________________________
int DRS_ENCODE::drs_decode_voltage_mode11(const int dsize,
                                          const unsigned short first_bin,
                                          const void *__restrict__ pdata,
                                          double *__restrict__ v,
                                          int *__restrict__ rebinSizeArray)
{
/*
Inputs
  dsize  : number of byte of encoded data
  pdata  : encoded data
Outputs
  v      : array of voltage
Return   : number of bins of DRS
*/
   unsigned int ibin;
   short sdata;

   if (v == 0) {
      return 0;
   }

   if (dsize <= 0 || pdata == 0) {
#ifdef RESET_WF
      memset(v, 0, kDRSBins * sizeof(double));
#endif
      return 0;
   }

   ibin = first_bin;
   int jbin;
   short rebinLevel = 0;
   double ddata;
   int idata = 0;
   int rebinSize = 1;
   int rebinSizeArrayPos = 0;
   const short *ps = static_cast<const short*>(pdata);
   const int dsizeHalf = dsize / 2;

   while (idata < dsizeHalf) {
      sdata = ps[idata];
#ifndef DRS_LITTLE_ENDIAN
      drs_word_swap(&sdata);
#endif
      idata++;
      if ((sdata & kMode11RebinChangeMask) == kMode11RebinChangeFlag) {
         rebinLevel = sdata & ~kMode11RebinChangeMask;
         rebinSize = 1 << rebinLevel;
         sdata = ps[idata];
#ifndef DRS_LITTLE_ENDIAN
         drs_word_swap(&sdata);
#endif
         idata++;
      }
      if (rebinSizeArray) {
         rebinSizeArray[rebinSizeArrayPos++] = rebinSize;
      }
      ddata = sdata * (1. / DRS_CF_V_MODE11);
      for (jbin = rebinSize; jbin > 0; jbin--) {
         v[ibin++] = ddata;
      }
   }
   if (rebinSizeArray && rebinSizeArrayPos < kDRSBins) {
      rebinSizeArray[rebinSizeArrayPos] = 0;
   }

   int nbin = ibin - first_bin;

#ifdef RESET_WF
   memset(v, 0, first_bin * sizeof(double));
   memset(v + first_bin + nbin, 0, (kDRSBins - (first_bin + nbin)) * sizeof(double));
#endif

   return nbin;
}


/*******************************************************************************
 * DRS time encoding mode 0
 * no compression
 * time difference is stored in char [%] relative to base time interval
 *******************************************************************************/
const double DRS_CF_T_MODE00 = 1e2;     // conversion factor for time

//______________________________________________________________________________
template <class T>
int DRS_ENCODE::drs_encode_time_mode00_template(const unsigned int nbin,
                                                const unsigned short dt,
                                                const unsigned short first_bin,
                                                const T *__restrict__ t,
                                                void *__restrict__ pdata)
{
/*
Inputs
  nbin   : number of bins of DRS
  dt : base time bin width [psec]
  t      : array of time
Outputs
  pdata  : encoded data
Return   : number of byte of encoded data
*/
   unsigned int ibin;
   int idata = 0;
   double ct;
   int   intdata;
   const double dt_dbl = dt * 1e-12;    // pico sec
   const double dt_dbl_inverse = 1. / dt_dbl;
   const T *pt = t + first_bin;
#if defined(VLA_AVAILABLE)
   double encoded[nbin];
   double decoded[nbin];
   T pt_pre[nbin];
#else
   double *encoded;
   double *decoded;
   T *pt_pre;
#endif

   if (nbin == 0 || t == 0 || pdata == 0) {
      return 0;
   }

#if !defined(VLA_AVAILABLE)
   encoded = new double[nbin];
   decoded = new double[nbin];
   pt_pre = new T[nbin];
#endif
   char *pc = static_cast<char*>(pdata);

   memcpy(pt_pre + 1, pt, (nbin - 1) * sizeof(T));
   ct = pt[0];

   if (first_bin != 0) {
      // store time of the first_bin
      intdata = static_cast<int>(ct * 1e12);        // [psec]
#ifndef DRS_LITTLE_ENDIAN
      drs_dword_swap(&intdata);
#endif
      memcpy(pc + idata, &intdata, sizeof(int));
      idata += sizeof(int);
   }

   if (nbin == 1) {
      // write flag which indicates nbin==1
      pc[idata++] = -1 * DRS_ENCODE::kMaxChar;
#if !defined(VLA_AVAILABLE)
      delete [] encoded;
      delete [] decoded;
      delete [] pt_pre;
#endif
      return idata;
   }

   // this loop can be vectorized
   for (ibin = 1; ibin < nbin; ibin++) {
      encoded[ibin] = (pt[ibin] - pt_pre[ibin] - dt_dbl) * dt_dbl_inverse *
            DRS_CF_T_MODE00;
   }
   for (ibin = 1; ibin < nbin; ibin++) {
      // equivalent to floor
      encoded[ibin] =
            encoded[ibin] > 0. ?
            static_cast<double>(static_cast<int>(encoded[ibin])) :
            static_cast<double>((static_cast<int>(encoded[ibin]) - 1));
   }
   for (ibin = 1; ibin < nbin; ibin++) {
      decoded[ibin] = dt_dbl * (1. + encoded[ibin] * (1. / DRS_CF_T_MODE00));
   }
   for (ibin = 1; ibin < nbin; ibin++) {
      ct = ct + decoded[ibin];
      pc[idata] = DRS_NCHAR(encoded[ibin]);
      if (fabs(pt[ibin] - ct) > dt_dbl * (0.5 / DRS_CF_T_MODE00)) {
         ct += dt_dbl * (1. / DRS_CF_T_MODE00);
         pc[idata]++;
      }
      idata++;
   }

#if !defined(VLA_AVAILABLE)
   delete [] encoded;
   delete [] decoded;
   delete [] pt_pre;
#endif

   return idata;
}

//______________________________________________________________________________
int DRS_ENCODE::drs_decode_time_mode00(const int dsize, const unsigned short dt,
                                       const unsigned short first_bin,
                                       const void *__restrict__ pdata,
                                       double *__restrict__ t)
{
/*
Inputs
  dsize     : number of byte of encoded data
  dt        : base time bin width [psec]
  first_bin : bin number of the first element of encoded data
  pdata     : encoded data
Outputs
  t      : array of time
Return   : number of bins of DRS
*/
   int nbin = dsize + 1;
   int ibin;
   double ct = 0;
   const double dt_dbl = dt * 1e-12;    // pico sec
   int idata;
   double *pt = t + first_bin;

   if (t == 0) {
      return 0;
   }

   if (dsize <= 0 || pdata == 0) {
#ifdef RESET_WF
      memset(t, 0, kDRSBins * sizeof(double));
#endif
      return 0;
   }

   const char* pc = static_cast<const char*>(pdata);

   if (first_bin != 0) {
      // read the time of the first_bin
      memcpy(&idata, pc, sizeof(int));
#ifndef DRS_LITTLE_ENDIAN
      drs_dword_swap(&idata);
#endif
      ct = idata * 1e-12;
      pc += sizeof(int);
      nbin -= sizeof(int);
   }

   // check if nbin is 1
   if (nbin == 2 && *pc == -1 * DRS_ENCODE::kMaxChar) {
      pt[0] = ct;
#ifdef RESET_WF
      memset(t + 1, 0, (kDRSBins - 1) * sizeof(double));
#endif
      return 1;
   }

   pt[0] = ct;

   for (ibin = 1; ibin < nbin; ibin++) {
      ct = pt[ibin] = ct + dt_dbl * (1. + (*pc) * (1. / DRS_CF_T_MODE00));
      pc++;
   }

#ifdef RESET_WF
   memset(t, 0, first_bin * sizeof(double));
   memset(t, first_bin + nbin, (kDRSBins - (first_bin + nbin)) * sizeof(double));
#endif

   return nbin;
}

//______________________________________________________________________________
int DRS_ENCODE::drs_encode_time_mode00(const unsigned int nbin,
                                       const unsigned short dt,
                                       const unsigned short first_bin,
                                       const double *__restrict__ t,
                                       void *__restrict__ pdata)
{
   return drs_encode_time_mode00_template(nbin, dt, first_bin, t, pdata);
}

//______________________________________________________________________________
int DRS_ENCODE::drs_encode_time_mode00f(const unsigned int nbin,
                                        const unsigned short dt,
                                        const unsigned short first_bin,
                                        const float *__restrict__ t,
                                        void *__restrict__ pdata)
{
   return drs_encode_time_mode00_template(nbin, dt, first_bin, t, pdata);
}


/*******************************************************************************
 * DRS time encoding mode 8
 * arrays of t is stored without any operation
 *******************************************************************************/

//______________________________________________________________________________
int DRS_ENCODE::drs_encode_time_mode08(const unsigned int nbin,
                                       const unsigned short first_bin,
                                       const double *__restrict__ t,
                                       void *__restrict__ pdata)
{
/*
Inputs
  nbin   : number of bins of DRS
  t      : array of time
Outputs
  pdata  : encoded data
Return   : number of byte of encoded data
*/
#ifndef DRS_LITTLE_ENDIAN
   unsigned int ibin;
#endif

   if (nbin == 0 || t == 0 || pdata == 0) {
      return 0;
   }

   memcpy(pdata, t + first_bin, sizeof(double) * nbin);

#ifndef DRS_LITTLE_ENDIAN
   double *pd = static_cast<double*>(pdata);
   for (ibin = 0; ibin < nbin; ibin++) {
      drs_qword_swap(pd + ibin);
   }
#endif

   return sizeof(double) * nbin;
}

//______________________________________________________________________________
int DRS_ENCODE::drs_decode_time_mode08(const int dsize,
                                       const unsigned short first_bin,
                                       const void *__restrict__ pdata,
                                       double *__restrict__ t)
{
/*
Inputs
  dsize  : number of byte of encoded data
  pdata  : encoded data
Outputs
  t      : array of time
Return   : number of bins of DRS
*/
   const int nbin = dsize / sizeof(double);
#ifndef DRS_LITTLE_ENDIAN
   int ibin;
   const int last_bin = first_bin + nbin;
#endif

   if (t == 0) {
      return 0;
   }

   if (dsize <= 0 || pdata == 0) {
#ifdef RESET_WF
      memset(t, 0, kDRSBins * sizeof(double));
#endif
      return 0;
   }

   memcpy(t + first_bin, pdata, sizeof(double) * nbin);

#ifndef DRS_LITTLE_ENDIAN
   for (ibin = first_bin; ibin < last_bin; ibin++) {
      drs_qword_swap(t + ibin);
   }
#endif

#ifdef RESET_WF
   memset(t, 0, first_bin * sizeof(double));
   memset(t, first_bin + nbin, (kDRSBins - (first_bin + nbin)) * sizeof(double));
#endif

   return nbin;
}

//______________________________________________________________________________
int DRS_ENCODE::drs_encode_time_mode08f(const unsigned int nbin,
                                        const unsigned short first_bin,
                                        const float *__restrict__ t,
                                        void *__restrict__ pdata)
{
   double t_dbl[kDRSBins];
   unsigned int i;
   const unsigned int last_bin = first_bin + nbin;

   if (nbin == 0 || t == 0 || pdata == 0) {
      return 0;
   }

   for (i = first_bin; i < last_bin; i++) {
      t_dbl[i] = t[i];
   }
   return drs_encode_time_mode08(nbin, first_bin, t_dbl, pdata);
}


/*******************************************************************************
 * DRS time encoding mode 9
 * arrays of t is stored after casting to float
 *******************************************************************************/

//______________________________________________________________________________
int DRS_ENCODE::drs_encode_time_mode09f(const unsigned int nbin,
                                        const unsigned short first_bin,
                                        const float *__restrict__ t,
                                        void *__restrict__ pdata)
{
/*
Inputs
  nbin   : number of bins of DRS
  t      : array of time
Outputs
  pdata  : encoded data
Return   : number of byte of encoded data
*/
#ifndef DRS_LITTLE_ENDIAN
   unsigned int ibin;
#endif

   if (nbin == 0 || t == 0 || pdata == 0) {
      return 0;
   }

   memcpy(pdata, t + first_bin, sizeof(float) * nbin);

#ifndef DRS_LITTLE_ENDIAN
   float *pf = static_cast<float*>(pdata);
   for (ibin = 0; ibin < nbin; ibin++) {
      drs_dword_swap(pf + ibin);
   }
#endif

   return sizeof(float) * nbin;
}

//______________________________________________________________________________
int DRS_ENCODE::drs_decode_time_mode09(const int dsize,
                                       const unsigned short first_bin,
                                       const void *__restrict__ pdata,
                                       double *__restrict__ t)
{
/*
Inputs
  dsize  : number of byte of encoded data
  pdata  : encoded data
Outputs
  t      : array of time
Return   : number of bins of DRS
*/
   int ibin;
   float fdata;
   const int nbin = dsize / sizeof(float);
   double *pt = t + first_bin;

   if (t == 0) {
      return 0;
   }

   if (dsize <= 0 || pdata == 0) {
#ifdef RESET_WF
      memset(t, 0, kDRSBins * sizeof(double));
#endif
      return 0;
   }

   const float *pf = static_cast<const float*>(pdata);
   for (ibin = 0; ibin < nbin; ibin++) {
      memcpy(&fdata, pf + ibin, sizeof(float));
#ifndef DRS_LITTLE_ENDIAN
      drs_dword_swap(&fdata);
#endif
      pt[ibin] = fdata;
   }

#ifdef RESET_WF
   memset(t, 0, first_bin * sizeof(double));
   memset(t, first_bin + nbin, (kDRSBins - (first_bin + nbin)) * sizeof(double));
#endif

   return nbin;
}

//______________________________________________________________________________
int DRS_ENCODE::drs_encode_time_mode09(const unsigned int nbin,
                                       const unsigned short first_bin,
                                       const double *__restrict__ t,
                                       void *__restrict__ pdata)
{
   float ft[kDRSBins];
   unsigned int i;
   const unsigned int last_bin = first_bin + nbin;

   if (nbin == 0 || t == 0 || pdata == 0) {
      return 0;
   }

   for (i = first_bin; i < last_bin; i++) {
      ft[i] = static_cast<float>(t[i]);
   }
   return drs_encode_time_mode09f(nbin, first_bin, ft, pdata);
}


//______________________________________________________________________________
int DRS_ENCODE::decode_rebin(int *rebinSize, double *timeSrc, double *timeDst,
                             double *ampSrc, double *ampDst)
{
   // calculate time and voltage for re-binned data
   int iBinSrc = 0;
   int iBinDst = 0;
   int iRebinSize = 0;
   while(iRebinSize < kDRSBins && rebinSize[iRebinSize] != 0) {
      timeDst[iBinDst] = drs_mean(rebinSize[iRebinSize], timeSrc + iBinSrc);
      if (ampDst && ampSrc) {
         ampDst[iBinDst] = ampSrc[iBinSrc];
      }
      iBinDst++;
      iBinSrc += rebinSize[iRebinSize];
      iRebinSize++;
   }
   return iBinDst;
}
