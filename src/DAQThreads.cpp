#include "DAQThreads.h"

//Midas Produced- DAQThread for Midas bank formatting
MidasProducer::MidasProducer(DAQBuffer<WDAQEvent> *source, WDSystem* system, EQUIPMENT* equipment, int index, DAQSystem* parent) : DAQThread(parent, "MidasProducer"){
   fSource = source;
   fEquipment = equipment;
   fMidasThreadIndex = index;
   fSystem = system;

   SetBufferWaitDuration(std::chrono::microseconds(100));
};
//Shared variabled, with mutex for access
std::mutex MidasProducer::fSharedMutex;
DWORD MidasProducer::fScalerTime;

//Setup ring buffers and shared variables, once thread start
void MidasProducer::Setup(){
   //Obtain ring buffer for inter-thread data exchange
   fRbh = get_event_rbh(fMidasThreadIndex);

   //first thread updates the shared variables
   if(fMidasThreadIndex == 0){
      std::lock_guard<std::mutex> guard(fSharedMutex);
      fScalerTime = ss_millitime();
   }
}

// Resets at begin of run
void MidasProducer::Begin(){
   //reset statistics
   fDroppedEvents = 0;
}

//Main thread loop
void MidasProducer::Loop(){
   WDAQEvent *ptr = nullptr;

   //try get an event from the DAQ
   if(fSource->Try_pop(ptr)){
      EVENT_HEADER *pevent;
      WORD *pdata;
      int  status;

      // obtain buffer space
      status = rb_get_wp(fRbh, (void **)&pevent, fBufferWaitDuration.count());
      if (status == DB_TIMEOUT) {
         //try gain after a while
         std::this_thread::yield();
         status = rb_get_wp(fRbh, (void **)&pevent, fBufferWaitDuration.count());
      }

      //if no buffer, panic!
      if (status != DB_SUCCESS){
         fDroppedEvents++;
         delete ptr;
         return;
      }

      //Mutex protect to multi-thread access to global variables
//      unsigned int serial;
      bool writeScaler = false;
      {
         std::lock_guard<std::mutex> guard(fSharedMutex);
         // this is needed if the serial number is thread safely assigned in this lib
         // serial = fEquipment->serial_number++;
         
         DWORD currentTime = ss_millitime();
         if((currentTime - fScalerTime) > 1000){
            writeScaler = true;
            fScalerTime = currentTime;
         }
      }
      
      // this is needed if the serial number is thread safely assigned in this lib
      // bm_compose_event(pevent, fEquipment->info.event_id, fEquipment->info.trigger_mask, 0, serial);

      // this is needed to have a thread safe serial number from midas lib
      bm_compose_event_threadsafe(pevent, fEquipment->info.event_id, fEquipment->info.trigger_mask, 0, &fEquipment->serial_number);

      pdata = (WORD *)(pevent + 1);

      // init bank structure
      bk_init32(pdata);

      //readout event here
      Readout(ptr, pdata);

      //write scaler if timer elapsed
      if(writeScaler){
         //readout scaler here
         ReadoutScaler(ptr, pdata);
      }

      pevent->data_size = bk_size(pdata);
      //printf("produced %lu size\n", pevent->data_size);

      // send event(s) to ring buffer
      rb_increment_wp(fRbh, sizeof(EVENT_HEADER) + pevent->data_size);

      //free space
      delete ptr;
   }
} 

//Printout at end of run
void MidasProducer::End(){
   //print statistics
   printf("could not push %lu events into buffer\n", fDroppedEvents);
}

// Event Readout Function, provides bank formatting
void MidasProducer::Readout(WDAQEvent* event_ptr, WORD* pevent){
   unsigned char *puchar;

   //extract board map for this event
   std::map<unsigned short, WDAQBoardEvent *> &wdb_map = event_ptr->fBoard[BRD_TYPE_ID_WDB];
   std::map<unsigned short, WDAQBoardEvent *> &tcb_map = event_ptr->fBoard[BRD_TYPE_ID_TCB];

   //DRS readout
   bk_create(pevent, "DRSV", TID_BYTE, (void **)&puchar);
   //Get WDBs in event
   for(auto it : wdb_map){
      unsigned short boardId = it.first;
      WDAQWdbEvent *boardEvent = static_cast<WDAQWdbEvent*>(it.second);

      //find zero suppression data for that board
      bool enableSwZeroSuppression = false;
      bool enableMaxMinCalculation = false;
      bool enableRebinning         = false;
      MidasZeroSuppressionConfig zeroConfig;
      {
         std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);
         auto pos = fSwZeroSuppressionConfig.find(boardId);
         if(pos != fSwZeroSuppressionConfig.end()){
            zeroConfig = pos->second;
            if(zeroConfig.Prescaling){
               enableSwZeroSuppression = true;
               //disable for a given prescaling
               if(zeroConfig.Prescaling !=1 && (event_ptr->mEventNumber % zeroConfig.Prescaling)==0)
                  enableSwZeroSuppression = false;

               enableRebinning = zeroConfig.RebinFactor>0;
               //decide if should compute min-max
               enableMaxMinCalculation = (zeroConfig.WfmSuppressionThr > 0);
               for(int iThr=0; iThr<MidasZeroSuppressionNRebinThr; iThr++){
                  if(zeroConfig.RebinThr[iThr] > 0) {
                     enableMaxMinCalculation = true;
                     enableRebinning = true;
                  }
               } // end for number of thr
               if(zeroConfig.RebinSignalThr>0)
                  enableMaxMinCalculation = true;

            } // end if prescaling
         } 
      }

      //loop on waveforms
      for(int iCh=0; iCh<WD_N_CHANNELS; iCh++){
         int chip = (iCh<16)? iCh/8: iCh-16;
         int channelOnChip = (iCh<16)? iCh%8: 8;
         int crate = boardEvent->mCrateId;
         int slot = boardEvent->mSlotId;
         int drs_voltage_flags = DRSH_VERSION4_2;
         unsigned short firstbin = 0;
         unsigned int nbin = 1024;
         int dsize;
         float min = 0.;
         float max = 1.;
         int rebinmethod = 1;
    
         //check has data
         if(boardEvent->mDrsHasData[iCh]){
            //encode wf to get size
            if(enableSwZeroSuppression){

               // all inputs (NO CLKS)
               if(iCh<16){
                  nbin = zeroConfig.SignalROIEnd-zeroConfig.SignalROIStart+1;
                  firstbin = zeroConfig.SignalROIStart;

                  // check if wfm suppression is actived :: i.e. thr != 0
                  if(enableMaxMinCalculation) {
                     // find min and max if any data suppression is activated
                     min = boardEvent->mDrsU[iCh][firstbin]; // reset value
                     max = boardEvent->mDrsU[iCh][firstbin]; 

                     for (int ibin = firstbin + 1; ibin <= zeroConfig.SignalROIEnd; ibin++) {
                        if(boardEvent->mDrsU[iCh][ibin]<min)
                           min = boardEvent->mDrsU[iCh][ibin];
                        if(boardEvent->mDrsU[iCh][ibin]>max)
                           max = boardEvent->mDrsU[iCh][ibin];
                     }
                     // if min-max is smaller larger (signed!) than thr then skip the wfm
                     // if zeroConfig.WfmSuppressionThr = 0 should never trigger the condition
                     if((max-min) < zeroConfig.WfmSuppressionThr) 
                        continue;
                  } 
       
                  if(enableRebinning){
                     //signal with rebin
                     short par[6];
                     par[0] = zeroConfig.RebinFactor;
                     //no pulse and full wfm rebinnig by using rebinmethod in the call
                     if((max-min) < zeroConfig.RebinSignalThr) {
                        rebinmethod = 0;
                     } else {
                        // number of regions
                        par[1] = 0;
                        int ipar = 2;
                        //first rebin region: ROI Start -> Signal Start
                        if(zeroConfig.SignalROIStart < zeroConfig.RegionStart){
                           par[1]++;
                           par[ipar++] = zeroConfig.SignalROIStart;
                           par[ipar++] = zeroConfig.RegionStart;
                        }
                        //second rebin region: Signal End -> ROI End
                        if(zeroConfig.SignalROIEnd > (zeroConfig.RegionEnd+1)){
                           par[1]++;
                           par[ipar++] = zeroConfig.RegionEnd+1;
                           par[ipar++] = zeroConfig.SignalROIEnd;
                        }

                        //retune the rebin factor with pulse height
                        for(int iThr=0; iThr<MidasZeroSuppressionNRebinThr; iThr++){
                           if((max-min) < zeroConfig.RebinThr[iThr])
                              par[0]++;
                        }
                     }

                     dsize = drs_encode_voltage_mode11f(nbin, firstbin,
                                                        boardEvent->mDrsU[iCh],
                                                        puchar +sizeof(DRS_CHANNELV_HEADER2),
                                                        rebinmethod,
                                                        par,
                                                        0); //should give time array here

                     drs_voltage_flags |= DRSH_ENC_MODE11;
                  } else {
                     //signal without rebinning
                     dsize = drs_encode_voltage_mode00f(nbin, firstbin,
                                                        boardEvent->mDrsU[iCh],
                                                        puchar + sizeof(DRS_CHANNELV_HEADER2));

                     drs_voltage_flags |= DRSH_ENC_MODE00;
                  }
               } else {
                  //clock
                  nbin = zeroConfig.ClkROIEnd-zeroConfig.ClkROIStart+1;
                  firstbin = zeroConfig.ClkROIStart;
                  dsize = drs_encode_voltage_mode00f(nbin, firstbin,
                                                     boardEvent->mDrsU[iCh],
                                                     puchar + sizeof(DRS_CHANNELV_HEADER2));

                  drs_voltage_flags |= DRSH_ENC_MODE00;
               }
            } else {
               // no zero suppression
               dsize = drs_encode_voltage_mode00f(nbin, firstbin,
                                                  boardEvent->mDrsU[iCh],
                                                  puchar + sizeof(DRS_CHANNELV_HEADER2));

               drs_voltage_flags |= DRSH_ENC_MODE00;
            }

            //compose header
            drs_compose_channelv_header2(puchar, drs_voltage_flags, crate, slot, chip, channelOnChip, 
                 dsize, firstbin, boardEvent->mTemperature);

            // increment for header
            puchar += sizeof(DRS_CHANNELV_HEADER2) + dsize;
         }
      }

   }
   bk_close(pevent, puchar);

   // add time calibration array to bank for the first event.
   // only trigger cell for readout boards in other events
   bk_create(pevent, "DRST", TID_BYTE, (void **)&puchar);

   //loop on all WDB in the system
   int drs_time_flags = DRSH_VERSION4_2 | DRSH_ENC_MODE09;
   if (SERIAL_NUMBER(pevent) == 0) {
      for(auto c : *fSystem)
         for(auto b :*c)
            if(b){
               if(dynamic_cast<WDWDB*>(b) != nullptr){
                  WDWDB* boardObj = static_cast<WDWDB*>(b);
                  unsigned short boardId = boardObj->GetSerialNumber();

                  //check for valid TCALIB
                  TCALIB* calib = &(boardObj->mTCalib);
                  if(! GetSystem()->GetAlarms()->Test(WDAQLIB_ERROR_MISSINGCALIB)){
                     if(! calib->IsValid()){
                        const std::string alarmMessage = "Missing missing calibration for board " + std::to_string(boardId);

                        GetSystem()->GetAlarms()->Trigger(WDAQLIB_ERROR_MISSINGCALIB, alarmMessage);
                     }
                  }

                  //loop on all channel
                  for(int iCh=0; iCh<WD_N_CHANNELS; iCh++){
                     int chip = (iCh<16)? iCh/8: iCh-16;
                     int channelOnChip = (iCh<16)? iCh%8: 8;
                     int crate = boardObj->GetCrateId();
                     int delta_t = (unsigned short) (1000000. / boardObj->GetDrsSampleFreqMhz());
                     int slot = boardObj->GetSlotId();

                     //encode time array
                     int dsize = drs_encode_time_mode09f(1024,
                                                         0, boardObj->mTCalib.mCalib.dt[iCh],
                                                         puchar + sizeof(DRS_CHANNELT_HEADER));

                     //try to get trigger cell from current event (if available)
                     int triggerCell = 0;
                     auto it = wdb_map.find(boardId);
                     if(it != wdb_map.end()){
                        //found, extract trigger cell from event
                        WDAQWdbEvent *boardEvent = static_cast<WDAQWdbEvent*>(it->second);
                        if(boardEvent->mDrsHasData[iCh]){
                           triggerCell = boardEvent->mTriggerCell[iCh];
                        }
                     }

                     //add header
                     drs_compose_channelt_header(puchar, drs_time_flags, crate, slot,
                                                 chip, channelOnChip, dsize, 0, triggerCell,
                                                 delta_t);

                     // increment for header
                     puchar += sizeof(DRS_CHANNELT_HEADER) + dsize;
                  }
               }
            }
   } else {
      //loop on all boards in the event
      for(auto it : wdb_map){
         //unsigned short boardId = it.first;
         WDAQWdbEvent *boardEvent = static_cast<WDAQWdbEvent*>(it.second);

         //loop on all channel
         for(int iCh=0; iCh<WD_N_CHANNELS; iCh++){
            int chip = (iCh<16)? iCh/8: iCh-16;
            int channelOnChip = (iCh<16)? iCh%8: 8;
            int crate = boardEvent->mCrateId;
            int slot = boardEvent->mSlotId;

            if(boardEvent->mDrsHasData[iCh]){
               //generate header
               int delta_t = (unsigned short) (1000000000. / boardEvent->mSamplingFrequency);
               drs_compose_channelt_header(puchar, drs_time_flags, crate, slot,
                                           chip, channelOnChip, 0, 0, boardEvent->mTriggerCell[iCh], 
                                           delta_t);

               //increment readout pointer
               puchar += sizeof(DRS_CHANNELT_HEADER);
            }
         }
      }
   }
   bk_close(pevent, puchar);

   //ADC readout
   bk_create(pevent, "ADCW", TID_BYTE, (void **)&puchar);
   for(auto it : wdb_map){
      unsigned short boardId = it.first;
      WDAQWdbEvent *boardEvent = static_cast<WDAQWdbEvent*>(it.second);

      bool enableSwZeroSuppression = true;
      MidasZeroSuppressionConfig zeroConfig;
      {
         std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);
         auto pos = fSwZeroSuppressionConfig.find(boardId);
         if(pos != fSwZeroSuppressionConfig.end()){
            zeroConfig = pos->second;
            /*if(zeroConfig.Prescaling){
               enableSwZeroSuppression = true;
               //disable for a given prescaling
               if(zeroConfig.Prescaling !=1 && (event_ptr->mEventNumber % zeroConfig.Prescaling)==0)
                  enableSwZeroSuppression = false;
            } // end if prescaling */
         } 
      }

      unsigned short firstbin = 0;
      unsigned int nbin = 2048;
      if(enableSwZeroSuppression){
         nbin = zeroConfig.AdcROIEnd-zeroConfig.AdcROIStart+1;
         firstbin = zeroConfig.AdcROIStart;
      }

      //loop on waveforms
      for(int iCh=0; iCh<WD_N_CHANNELS; iCh++){
         int chip = (iCh<16)? iCh/8: iCh-16;
         int channelOnChip = (iCh<16)? iCh%8: 8;
         int crate = boardEvent->mCrateId;
         int slot = boardEvent->mSlotId;
         int drs_voltage_flags = DRSH_VERSION4_2 | DRSH_ENC_MODE15;

         //check has data
         if(boardEvent->mAdcHasData[iCh]){
            //encode wf to get size
            unsigned short* ptr = (unsigned short*)(puchar + sizeof(DRS_CHANNELV_HEADER));
            //int nBin = boardEvent->mAdcByteNumber[iCh] / 12; // 12 = 1.5 bytes * 8
            for(int iBin=0; iBin<nbin; iBin++){
               *(ptr++) = boardEvent->mAdcU[iCh][firstbin+iBin];
            }
            //compose header
            drs_compose_channelv_header(puchar, drs_voltage_flags, crate, slot, chip, channelOnChip, nbin*sizeof(unsigned short), 0);

            //increment readout pointer
            puchar += sizeof(DRS_CHANNELV_HEADER) + nbin*sizeof(unsigned short);

         }
      }
   }
   bk_close(pevent, puchar);

   //TDC readout
   bk_create(pevent, "TDCW", TID_BYTE, (void **)&puchar);
   for(auto it : wdb_map){
      //unsigned short boardId = it.first;
      WDAQWdbEvent *boardEvent = static_cast<WDAQWdbEvent*>(it.second);

      //loop on waveforms
      for(int iCh=0; iCh<WD_N_CHANNELS; iCh++){
         int chip = (iCh<16)? iCh/8: iCh-16;
         int channelOnChip = (iCh<16)? iCh%8: 8;
         int crate = boardEvent->mCrateId;
         int slot = boardEvent->mSlotId;
         int drs_voltage_flags = DRSH_VERSION4_2 | DRSH_ENC_MODE15;

         //check has data
         if(boardEvent->mTdcHasData[iCh]){
              unsigned char* ptr = puchar + sizeof(DRS_CHANNELV_HEADER);
              for(int iBin=0; iBin<512; iBin++){
                *(ptr++) = boardEvent->mTdc[iCh][iBin];
              }
            //compose header
            drs_compose_channelv_header(puchar, drs_voltage_flags, crate, slot, chip, channelOnChip, 512, 0);

            //increment readout pointer
            puchar += sizeof(DRS_CHANNELV_HEADER) + 512;
         }
      }
   }
   bk_close(pevent, puchar);

   //TRG readout from WDB
   bk_create(pevent, "TRGW", TID_BYTE, (void **)&puchar);
   for(auto it : wdb_map){
      //unsigned short boardId = it.first;
      WDAQWdbEvent *boardEvent = static_cast<WDAQWdbEvent*>(it.second);

      //check has data
      if(boardEvent->mTrgHasData){
         int crate = boardEvent->mCrateId;
         int slot = boardEvent->mSlotId;
         int drs_voltage_flags = DRSH_VERSION4_2 | DRSH_ENC_MODE15;

         unsigned long* ptr = (unsigned long*)(puchar + sizeof(DRS_CHANNELV_HEADER));
         for(int iBin=0; iBin<512; iBin++){
            *(ptr++) = boardEvent->mTrg[iBin];
         }
         drs_compose_channelv_header(puchar, drs_voltage_flags, crate, slot,
                                     3, 0, 512*sizeof(unsigned long), 0); //chip==3 for trigger
         //increment readout pointer
         puchar += sizeof(DRS_CHANNELV_HEADER) + 512*sizeof(unsigned long);
      }
   }
   bk_close(pevent, puchar);

   //TRGI of master trigger board
   try {
      WDBoard* triggerb = fSystem->GetTriggerBoard();
      if(triggerb){
         unsigned short triggerbID = triggerb->GetSerialNumber();
         WDAQTcbEvent *triggerEvent = static_cast<WDAQTcbEvent*>(tcb_map.at(triggerbID));
         std::array<char, 4> trgiName({'T', 'R', 'G', 'I'});
         WDAQTcbBank* trgiBank = triggerEvent->mBanks.at(trgiName);
         int crate = triggerEvent->mCrateId;
         int slot = triggerEvent->mSlotId;
         int drs_voltage_flags = DRSH_VERSION5 | DRSH_ENC_MODE15; //DRSH_VERSION5=TCB, DRSH_ENC_MODE15=custom
         bk_create(pevent, "TRGI", TID_BYTE, (void **)&puchar);
         unsigned int* ptr = (unsigned int*)(puchar + sizeof(DRS_CHANNELV_HEADER));
         for(unsigned int &v : trgiBank->data){
            *(ptr++) = v;
         }

         drs_compose_channelv_header(puchar, drs_voltage_flags, crate, slot,
                                     0, 0, trgiBank->data.size()*sizeof(unsigned int), 0);

         //increment readout pointer
         puchar += sizeof(DRS_CHANNELV_HEADER) + trgiBank->data.size()*sizeof(unsigned int);

         bk_close(pevent, puchar);
      }
   } catch (const std::out_of_range& ex){
      printf("out of range in TRGI readout\n");
      
   } catch (const std::runtime_error& ex){
      printf("runtime error in TRGI readout\n");
   }

   //TCB INPUT memories
   bk_create(pevent, "TINP", TID_BYTE, (void **)&puchar);
   std::array<char, 4> tinpName({'T', 'I', 'N', 'P'});
   for(auto it : tcb_map){
      //unsigned short boardId = it.first;
      WDAQTcbEvent *boardEvent = static_cast<WDAQTcbEvent*>(it.second);
      auto bankIter = boardEvent->mBanks.find(tinpName);
      if(bankIter != boardEvent->mBanks.end()){
         int crate = boardEvent->mCrateId;
         int slot = boardEvent->mSlotId;
         int drs_voltage_flags = DRSH_VERSION5 | DRSH_ENC_MODE15; //DRSH_VERSION5=TCB, DRSH_ENC_MODE15=custom

         WDAQTcbBank* bank = bankIter->second;

         unsigned int* ptr = (unsigned int*)(puchar + sizeof(DRS_CHANNELV_HEADER));
         for(unsigned int &v : bank->data){
            *(ptr++) = v;
         }

         drs_compose_channelv_header(puchar, drs_voltage_flags, crate, slot,
                                     0, 0, bank->data.size()*sizeof(unsigned int), 0);

         //increment readout pointer
         puchar += sizeof(DRS_CHANNELV_HEADER) + bank->data.size()*sizeof(unsigned int);

      }
   }
   bk_close(pevent, puchar);



   //TCB readout
   for(auto it : tcb_map){
      //unsigned short boardId = it.first;
      WDAQTcbEvent *boardEvent = static_cast<WDAQTcbEvent*>(it.second);
      int crate = boardEvent->mCrateId;
      int slot = boardEvent->mSlotId;

      //loop on banks for that board
      for(auto it2 : boardEvent->mBanks){
         WDAQTcbBank* bank = it2.second;
         char *bank_name = bank->fName.data();
         if(strncmp(bank_name, "TRGI", 4)==0){
            //TRGI bank only saved for TCB master
            //Here should check consistency of other banks
            continue;
         } else if(strncmp(bank_name, "TINP", 4)==0){
            // do not readout TINP twice
            continue;
         }
         int drs_voltage_flags = DRSH_VERSION5 | DRSH_ENC_MODE15; //DRSH_VERSION5=TCB, DRSH_ENC_MODE15=custom

         //create Midas bank
         bk_create(pevent, bank_name, TID_BYTE, (void **)&puchar);
         unsigned int* ptr = (unsigned int*)(puchar + sizeof(DRS_CHANNELV_HEADER));
         for(unsigned int &v : bank->data){
            *(ptr++) = v;
         }

         drs_compose_channelv_header(puchar, drs_voltage_flags, crate, slot,
                                     0, 0, bank->data.size()*sizeof(unsigned int), 0);

         //increment readout pointer
         puchar += sizeof(DRS_CHANNELV_HEADER) + bank->data.size()*sizeof(unsigned int);

         bk_close(pevent, puchar);
      }
   }
}

// Scaler Readout Function, provides bank formatting
void MidasProducer::ReadoutScaler(WDAQEvent* event_ptr, WORD* pevent){
   unsigned char *puchar;

   //extract board map for this event
   std::map<unsigned short, WDAQBoardEvent *> &wdb_map = event_ptr->fBoard[BRD_TYPE_ID_WDB];

   //scaler readout from WDB
   bk_create(pevent, "SCLR", TID_BYTE, (void **)&puchar);
   for(auto it : wdb_map){
      //unsigned short boardId = it.first;
      WDAQWdbEvent *boardEvent = static_cast<WDAQWdbEvent*>(it.second);

      //check has data
      if(boardEvent->mScalerHasData){
         int crate = boardEvent->mCrateId;
         int slot = boardEvent->mSlotId;
         int drs_voltage_flags = DRSH_VERSION4_2 | DRSH_ENC_MODE15;

         unsigned long* ptr = (unsigned long*)(puchar + sizeof(DRS_CHANNELV_HEADER));
         *(ptr++) = boardEvent->mTimeStamp;

         for(int iSca=3; iSca<16+3; iSca++){
            *(ptr++) = boardEvent->mScaler[iSca];
         }

         drs_compose_channelv_header(puchar, drs_voltage_flags, crate, slot,
                                     0, 0, 17*sizeof(unsigned long), 0);
         //increment readout pointer
         puchar += sizeof(DRS_CHANNELV_HEADER) + 17*sizeof(unsigned long);
      }
   }
   bk_close(pevent, puchar);
}

//Zero suppression settings with mutex
void MidasProducer::SetSwZeroSuppression(unsigned int board, unsigned short prescaling, unsigned short clkROIstart, unsigned short clkROIend, unsigned short signalROIstart, unsigned short signalROIend, unsigned short regionStart, unsigned short regionEnd, unsigned short rebinFactor, float rebinSignalThr, float wfmSuppressionThr, float* rebinThr, unsigned short adcROIstart, unsigned short adcROIend){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   fSwZeroSuppressionConfig[board].Prescaling = prescaling;
   fSwZeroSuppressionConfig[board].ClkROIStart = clkROIstart;
   fSwZeroSuppressionConfig[board].ClkROIEnd = clkROIend;
   fSwZeroSuppressionConfig[board].SignalROIStart = signalROIstart;
   fSwZeroSuppressionConfig[board].SignalROIEnd = signalROIend;
   fSwZeroSuppressionConfig[board].RegionStart = regionStart;
   fSwZeroSuppressionConfig[board].RegionEnd = regionEnd;
   fSwZeroSuppressionConfig[board].RebinFactor = rebinFactor;
   fSwZeroSuppressionConfig[board].RebinSignalThr = rebinSignalThr;
   fSwZeroSuppressionConfig[board].WfmSuppressionThr = wfmSuppressionThr;
   for(int iThr=0; iThr<MidasZeroSuppressionNRebinThr; iThr++){
      fSwZeroSuppressionConfig[board].RebinThr[iThr] = rebinThr[iThr];
   }
   fSwZeroSuppressionConfig[board].AdcROIStart = adcROIstart;
   fSwZeroSuppressionConfig[board].AdcROIEnd = adcROIend;
}

void MidasProducer::SetSwZeroSuppressionPrescaling(unsigned int board, unsigned short parameter){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   fSwZeroSuppressionConfig[board].Prescaling = parameter;
}

void MidasProducer::SetSwZeroSuppressionClkROIStart(unsigned int board, unsigned short parameter){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   fSwZeroSuppressionConfig[board].ClkROIStart = parameter;
}

void MidasProducer::SetSwZeroSuppressionClkROIEnd(unsigned int board, unsigned short parameter){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   fSwZeroSuppressionConfig[board].ClkROIEnd = parameter;
}

void MidasProducer::SetSwZeroSuppressionSignalROIStart(unsigned int board, unsigned short parameter){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   fSwZeroSuppressionConfig[board].SignalROIStart = parameter;
}

void MidasProducer::SetSwZeroSuppressionSignalROIEnd(unsigned int board, unsigned short parameter){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   fSwZeroSuppressionConfig[board].SignalROIEnd = parameter;
}

void MidasProducer::SetSwZeroSuppressionRegionStart(unsigned int board, unsigned short parameter){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   fSwZeroSuppressionConfig[board].RegionStart = parameter;
}

void MidasProducer::SetSwZeroSuppressionRegionEnd(unsigned int board, unsigned short parameter){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   fSwZeroSuppressionConfig[board].RegionEnd = parameter;
}

void MidasProducer::SetSwZeroSuppressionRebinFactor(unsigned int board, unsigned short parameter){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   fSwZeroSuppressionConfig[board].RebinFactor = parameter;
}

void MidasProducer::SetSwZeroSuppressionRebinSignalThr(unsigned int board, float parameter){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   fSwZeroSuppressionConfig[board].RebinSignalThr = parameter;
}

void MidasProducer::SetSwZeroSuppressionWfmSuppressionThr(unsigned int board, float parameter){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   fSwZeroSuppressionConfig[board].WfmSuppressionThr = parameter;
}

void MidasProducer::SetSwZeroSuppressionRebinThr(unsigned int board, float *parameter){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   for(int iThr=0; iThr<MidasZeroSuppressionNRebinThr; iThr++){
      fSwZeroSuppressionConfig[board].RebinThr[iThr] = parameter[iThr];
   }
}

void MidasProducer::SetSwZeroSuppressionAdcROIStart(unsigned int board, unsigned short parameter){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   fSwZeroSuppressionConfig[board].AdcROIStart = parameter;
}

void MidasProducer::SetSwZeroSuppressionAdcROIEnd(unsigned int board, unsigned short parameter){
   std::lock_guard<std::mutex> lg(fSwZeroSuppressionMutex);

   fSwZeroSuppressionConfig[board].AdcROIEnd = parameter;
}


