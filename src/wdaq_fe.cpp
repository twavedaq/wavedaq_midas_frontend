/********************************************************************\
 
  Name:         wdaq_fe.cpp
  Created by:   Marco Francesconi
 
  Contents:     Frontend program for MEG II WaveDAQ readout
 
 \********************************************************************/

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "midas.h"
#include "mfe.h"
#include "msystem.h"
#include "strlcpy.h"
#include "mscb.h"
#include "drs_encode/drs_encode.h"
#include "WDLib.h"
#include "DAQThreads.h"
//FIXME: keep this include aligned with others
#include "event_id.h"

#include <iostream>
#include <exception>
#include <stdexcept>
#include <set>
#include <vector>
#include <map>
#include <unordered_map>

/* make frontend functions callable from the C framework */

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
const char *frontend_name = "WDAQ Frontend";
/* The frontend file name, don't change it */
const char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0;

/* maximum event size produced by this frontend */
INT max_event_size = 128 * 1024 * 1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 2 * 8 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 4 * max_event_size;

extern INT verbosity_level;
extern INT run_state;
   
/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_scaler_event(char *pevent, INT off);

INT poll_event(INT source, INT count, BOOL test);
INT interrupt_configure(INT cmd, INT source, POINTER_T adr);

/*-- Equipment list ------------------------------------------------*/

BOOL equipment_common_overwrite = TRUE;

EQUIPMENT equipment[] = {

   { "Trigger",              // equipment name
      { EID_TRIGGER, 0,      // event ID, trigger mask
        "SYSTEM",            // event buffer
        EQ_USER,            // equipment type
        0,                   // event source
        "MIDAS",             // format
        TRUE,                // enabled
        RO_RUNNING,          // read only when running
        100,                 // poll for 100ms
        0,                   // stop run after this event limit
        0,                   // number of sub events
        1,                   // log history
        "", "", "", },
      NULL,                  // readout routine
   },

   { "Scaler",               // equipment name
      { EID_SCALER,          // event ID
         0,                  // trigger mask
         "SYSTEM",           // event buffer
         EQ_PERIODIC,        // equipment type
         0,                  // event source
         "MIDAS",            // format
         TRUE,               // enabled
         RO_RUNNING,         // read when running
         10000,              // read every 10 sec
         0,                  // stop run after this event limit
         0,                  // number of sub events
         0,                  // don-t log history
         "", "",  "",        // Reserved
      },
      read_scaler_event,     // readout routine
   },
   
   {""}
};

WDSystem* sys;
std::vector<MidasProducer *> midasThreads;

//time of last writing of ODB
DWORD threadStatTime;

// array of trg counter from previous event
u_int32_t trgcouprev[64];
u_int32_t ttimeprev, ltimeprev;

// Trigger counter normalisation"
// false -> live time
// true -> total time
bool  TCOUMODE[64] = {false, false, false, false, //0-3
  false, false, true, false, //4-7
  false, false, false, false, //8-11
  false, false, false, false, //12-15
  false, false, false, false, //16-19
  true,  true,  true,  true, //20-23
  true,  true, false, false, //24-27
  false, false, false, false, //28-31
  true, false,  true, false, //32-35
  false, false, false, false, //36-39
  true, true, true,  true, //40-43
  true, true, true,  true, //44-47
  true, true, false,  false, //48-51
  false, false, false, true, //52-55
  true, true, false, true, //56-59
  true, false, false, true}; //60-63


/*- Support functions ----------------------------------------------*/

// add a new entry to the ODB. note: data type needs to match TID_XXX in type parameter
INT AddToOdb(HNDLE hDB, HNDLE hKeyRoot, std::string crate, std::string board, std::string name, const void *data, INT size, INT n, DWORD type){
   char str[256];

   sprintf(str, "Settings/WaveDAQ/%s/%s/%s", crate.c_str(), board.c_str(), name.c_str());
   int ret = db_set_value(hDB, hKeyRoot, str, data, size, n, type);
   if (ret != DB_SUCCESS) {
      cm_msg(MERROR, "frontend_init", "Frontend error: Cannot write defaults to %s, db_set_value() returned %d", str, ret);
      return FE_ERR_ODB ;
   }

   return SUCCESS;
}

// converts an ODB key to a string to be stored in a property
std::string OdbToString(HNDLE hDB, HNDLE hKey){
   std::stringstream sstr;

   //read the key
   KEY key;
   db_get_key(hDB, hKey, &key);

   //read the data
   char buf[256];
   int size = sizeof(buf);
   char* ptr = buf;
   db_get_data(hDB, hKey, ptr, &size, key.type);

   //loops on values
   for(int i=0; i<key.num_values; i++){
      if(sstr.tellp()){
         sstr << ", ";
      }
      
      switch(key.type){
      case TID_BYTE:
         sstr << std::to_string(*((BYTE*)ptr));
         break;
      case TID_SBYTE:
         sstr << std::to_string(*((BYTE*)ptr));
         break;
      case TID_CHAR:
         sstr << std::to_string(*((char*)ptr));
         break;
      case TID_WORD:
         //sstr << std::to_string(*((WORD*)ptr));
         sstr << std::hex <<*((WORD*)ptr);
         break;
      case TID_SHORT:
         sstr << std::to_string(*((short*)ptr));
         break;
      case TID_DWORD:
         //sstr << std::to_string(*((DWORD*)ptr));
         sstr << std::hex <<*((DWORD*)ptr);
         break;
      case TID_INT:
         sstr << std::to_string(*((INT*)ptr));
         break;
      case TID_BOOL:
         if(key.num_values==1){
            if(*((DWORD*)ptr)==TRUE){
               sstr << "true";
            } else {
               sstr << "false";
            }
         } else {
            if(*((DWORD*)ptr)==TRUE){
               sstr << "1";
            } else {
               sstr << "0";
            }
         }
         break;
      case TID_FLOAT:
         sstr << std::to_string(*((float*)ptr));
         break;
      case TID_DOUBLE:
         sstr << std::to_string(*((double*)ptr));
         break;
      case TID_STRING:
         sstr << std::string((char*)ptr);
         break;

      //unsupported
      case TID_BITFIELD:
      case TID_ARRAY:
      case TID_STRUCT:
      case TID_KEY:
      case TID_LINK:
      case TID_LAST:
      default:
         break;
      }

      ptr += key.item_size;
   }
   return sstr.str();
}

void set_swzerosuppression_from_ODB(HNDLE hDB, HNDLE hKey, std::string &keyName, unsigned int boardId){
   if(keyName == "SwZeroSuppressionPrescaling"){
      unsigned short value;
      INT size = sizeof(value);
      db_get_data(hDB, hKey, &value, &size, TID_WORD);

      for(auto thread: midasThreads)
         thread->SetSwZeroSuppressionPrescaling(boardId, value);
   } else if(keyName == "SwZeroSuppressionClkROIStart"){
      unsigned short value;
      INT size = sizeof(value);
      db_get_data(hDB, hKey, &value, &size, TID_WORD);

      for(auto thread: midasThreads)
         thread->SetSwZeroSuppressionClkROIStart(boardId, value);
   } else if(keyName == "SwZeroSuppressionClkROIEnd"){
      unsigned short value;
      INT size = sizeof(value);
      db_get_data(hDB, hKey, &value, &size, TID_WORD);

      for(auto thread: midasThreads)
         thread->SetSwZeroSuppressionClkROIEnd(boardId, value);
   } else if(keyName == "SwZeroSuppressionSignalROIStart"){
      unsigned short value;
      INT size = sizeof(value);
      db_get_data(hDB, hKey, &value, &size, TID_WORD);

      for(auto thread: midasThreads)
         thread->SetSwZeroSuppressionSignalROIStart(boardId, value);
   } else if(keyName == "SwZeroSuppressionSignalROIEnd"){
      unsigned short value;
      INT size = sizeof(value);
      db_get_data(hDB, hKey, &value, &size, TID_WORD);

      for(auto thread: midasThreads)
         thread->SetSwZeroSuppressionSignalROIEnd(boardId, value);
   } else if(keyName == "SwZeroSuppressionRegionStart"){
      unsigned short value;
      INT size = sizeof(value);
      db_get_data(hDB, hKey, &value, &size, TID_WORD);

      for(auto thread: midasThreads)
         thread->SetSwZeroSuppressionRegionStart(boardId, value);
   } else if(keyName == "SwZeroSuppressionRegionEnd"){
      unsigned short value;
      INT size = sizeof(value);
      db_get_data(hDB, hKey, &value, &size, TID_WORD);

      for(auto thread: midasThreads)
         thread->SetSwZeroSuppressionRegionEnd(boardId, value);
   } else if(keyName == "SwZeroSuppressionRebinFactor"){
      unsigned short value;
      INT size = sizeof(value);
      db_get_data(hDB, hKey, &value, &size, TID_WORD);

      for(auto thread: midasThreads)
         thread->SetSwZeroSuppressionRebinFactor(boardId, value);
   } else if(keyName == "SwZeroSuppressionRebinSignalThr"){
      float value;
      INT size = sizeof(value);
      db_get_data(hDB, hKey, &value, &size, TID_FLOAT);

      for(auto thread: midasThreads)
         thread->SetSwZeroSuppressionRebinSignalThr(boardId, value);
   } else if(keyName == "SwZeroSuppressionThr"){
      float value;
      INT size = sizeof(value);
      db_get_data(hDB, hKey, &value, &size, TID_FLOAT);

      for(auto thread: midasThreads)
         thread->SetSwZeroSuppressionWfmSuppressionThr(boardId, value);
   } else if(keyName == "SwZeroSuppressionRebinThr"){
      float value[MidasZeroSuppressionNRebinThr];
      INT size = sizeof(value);
      db_get_data(hDB, hKey, value, &size, TID_FLOAT);

      for(auto thread: midasThreads)
         thread->SetSwZeroSuppressionRebinThr(boardId, value);
   } else if(keyName == "SwZeroSuppressionAdcROIStart"){
      unsigned short value;
      INT size = sizeof(value);
      db_get_data(hDB, hKey, &value, &size, TID_WORD);

      for(auto thread: midasThreads)
         thread->SetSwZeroSuppressionAdcROIStart(boardId, value);
   } else if(keyName == "SwZeroSuppressionAdcROIEnd"){
      unsigned short value;
      INT size = sizeof(value);
      db_get_data(hDB, hKey, &value, &size, TID_WORD);

      for(auto thread: midasThreads)
         thread->SetSwZeroSuppressionAdcROIEnd(boardId, value);
   }
}

Property &set_banks_from_ODB(HNDLE hDB, HNDLE hKey, WDBoard* b){
   //Get ODB Value
   BOOL value;
   INT size = sizeof(value);
   db_get_data(hDB, hKey, &value, &size, TID_BOOL);

   //Get Key Name
   KEY key;
   db_get_key(hDB, hKey, &key);
   std::string keyName(key.name);

   //Get Banks property
   Property &property = b->GetProperty("Banks");
   std::string list = property.GetStringValue();

   if(value==TRUE){
      //add to bank list
      if(list.find(keyName.substr(6))==std::string::npos){
         if(list.size()) list += ',';
         list += keyName.substr(6);
      }
   } else {
      //remove to bank list
      size_t pos = list.find(keyName.substr(6));
      if(pos==0) list.erase(pos, keyName.size()-5);
      else if(pos!=std::string::npos) list.erase(pos-1, keyName.size()-5);
   }
   property.SetStringValue(list);

   return property;
}

void set_trigger_from_ODB(HNDLE hDB, HNDLE hKey, WDBoard *b, bool configure){
   Property propertyEnable = b->GetProperty("TriggerEnable");
   Property propertyPrescaling = b->GetProperty("TriggerPrescaling");

   BOOL odbEnable[64];
   HNDLE hKeyEnable;
   INT size = sizeof(odbEnable);
   db_find_key(hDB, hKey, "TriggerEnable", &hKeyEnable);
   db_get_data(hDB, hKeyEnable, odbEnable, &size, TID_BOOL);

   DWORD odbPrescaling[64];
   HNDLE hKeyPrescaling;
   size = sizeof(odbPrescaling);
   db_find_key(hDB, hKey, "TriggerPrescaling", &hKeyPrescaling);
   db_get_data(hDB, hKeyPrescaling, odbPrescaling, &size, TID_DWORD);

   std::string strEnable;
   std::string strPrescaling;
   for(int i=0; i<64; i++){
      if(odbEnable[i]==TRUE){
         if(strEnable.size()) strEnable += ", ";
         strEnable += std::to_string(i);

         if(strPrescaling.size()) strPrescaling += ", ";
         strPrescaling += std::to_string(odbPrescaling[i]);
      }
   }

   b->AddProperty("TriggerEnable", strEnable);
   b->AddProperty("TriggerPrescaling", strPrescaling);
   try{
      if(configure) b->ConfigureProperty("TriggerEnable", b->GetProperty("TriggerEnable"));
      if(configure) b->ConfigureProperty("TriggerPrescaling", b->GetProperty("TriggerPrescaling"));
   } catch (const std::runtime_error& ex){
      cm_msg(MERROR, "set_trigger_from_ODB", "Configuration error: %s", ex.what());
      cm_disconnect_experiment();
      exit(0);
   }

   try{
      Property property = b->GetProperty("TriggerDelay");
      INT odbDelay[64];
      HNDLE hKeyDelay;
      size = sizeof(odbDelay);
      db_find_key(hDB, hKey, "TriggerDelay", &hKeyDelay);
      db_get_data(hDB, hKeyDelay, odbDelay, &size, TID_INT);

      std::string strDelay;
      for(int i=0; i<64; i++){
         if(odbEnable[i]==TRUE){
            if(strDelay.size()) strDelay += ", ";
            strDelay += std::to_string(odbDelay[i]);
         }
      }
      b->AddProperty("TriggerDelay", strDelay);
      try{
         if(configure) b->ConfigureProperty("TriggerDelay", b->GetProperty("TriggerDelay"));
      } catch (const std::runtime_error& ex){
         cm_msg(MERROR, "set_trigger_from_ODB", "Configuration error: %s", ex.what());
         cm_disconnect_experiment();
         exit(0);
      }

   } catch (const std::runtime_error& ex){
   }

}


/*- ODB stuff ------------------------------------------------------*/
// ODB hotlink
void crate_settings_changed(HNDLE hDB, HNDLE hKey, int index, void *){

   // indicate that update has been started
   HNDLE hKeyRoot;
   char str[256];
   sprintf(str, "/Equipment/%s", equipment->name);
   db_find_key(hDB, 0, str, &hKeyRoot);
   BOOL config_busy = TRUE;
   db_set_value(hDB, hKeyRoot, "Variables/Config busy", &config_busy, sizeof(config_busy), 1, TID_BOOL);

   set_equipment_status("Trigger", "Configuring ...", "#87CEFA");

   //get key
   KEY key;
   db_get_key(hDB, hKey, &key);
   //printf("name: %s index:%d\n", key.name, index);

   //get parent key
   HNDLE hParentKey;
   KEY parentKey;
   db_get_parent(hDB, hKey, &hParentKey);
   db_get_key(hDB, hParentKey, &parentKey);
   //printf("parent name: %s\n", parentKey.name);

   //converts to std::string and retrieve the WDBoard
   std::string keyName(key.name);
   std::string boardName(parentKey.name);
   WDBoard* b= sys->GetBoardAt(sys->FindBoard(boardName));

   //keys with custom treatment
   if(keyName=="TriggerEnable" || keyName=="TriggerPrescaling" || keyName=="TriggerDelay"){
      set_trigger_from_ODB(hDB, hParentKey, b, true);

   // check TCB Bank Write
   } else if(keyName.find("Write ")==0){
      Property& property = set_banks_from_ODB(hDB, hKey, b);
      b->ConfigureProperty("Banks", property);

   // software zero suppression
   } else if(keyName.find("SwZero")==0){
      set_swzerosuppression_from_ODB(hDB, hKey, keyName, b->GetSerialNumber());

   //ignore Slot
   } else if(keyName == "Slot"){

   //other keys
   } else {
      std::string str = OdbToString(hDB, hKey);
      printf("setting property %s to \"%s\"\n", key.name, str.c_str());

      b->AddProperty(keyName, str);
      try{
         b->ConfigureProperty(keyName, b->GetProperty(keyName));
      } catch (const std::runtime_error& ex){
         cm_msg(MERROR, "crate_settings_changed", "Configuration error: %s", ex.what());
         cm_disconnect_experiment();
         exit(0);
      }

   }

   // notify that the frontend is not busy any more
   config_busy = FALSE;
   db_set_value(hDB, hKeyRoot, "Variables/Config busy", &config_busy, sizeof(config_busy), 1, TID_BOOL);
   set_equipment_status("Trigger", "Ok", "greenLight");
}

// ODB hotlink
void reload_settings(HNDLE hDB, HNDLE hKey, int index, void *){
   printf("Reloading all from ODB.....\n");

   //find ODB root
   HNDLE hKeyRoot;
   char str[256];
   sprintf(str, "/Equipment/%s", equipment->name);
   db_find_key(hDB, 0, str, &hKeyRoot);

   //notify frontend busy
   BOOL config_busy = TRUE;
   db_set_value(hDB, hKeyRoot, "Variables/Config busy", &config_busy, sizeof(config_busy), 1, TID_BOOL);
   set_equipment_status("Trigger", "Configuring ...", "#87CEFA");

   //find WaveDAQ 
   HNDLE hWavedaqKey;
   db_find_key(hDB, hKeyRoot, "Settings/WaveDAQ/", &hWavedaqKey);

   //loop on crates
   for (int i=0 ; ; i++)
   {
      HNDLE hCrateKey;
      db_enum_key(hDB, hWavedaqKey, i, &hCrateKey);
      if (!hCrateKey)
         break; // end of list reached

      // loop on boards
      for (int j=0 ; ; j++)
      {
         HNDLE hBoardKey;
         db_enum_key(hDB, hCrateKey, j, &hBoardKey);
         if (!hBoardKey)
            break; // end of list reached
         KEY boardKey;
         db_get_key(hDB, hBoardKey, &boardKey);

         std::string boardName(boardKey.name);
         if(boardName != "MscbName"){
            WDBoard* b= sys->GetBoardAt(sys->FindBoard(boardName));

            // loop on properties
            for (int k=0 ; ; k++)
            {
               HNDLE hPropertyKey;
               db_enum_key(hDB, hBoardKey, k, &hPropertyKey);
               if (!hPropertyKey)
                  break; // end of list reached

               KEY propertyKey;
               db_get_key(hDB, hPropertyKey, &propertyKey);

               std::string propertyName(propertyKey.name);
               if(propertyName.find("Write ")==0){
                  set_banks_from_ODB(hDB, hPropertyKey, b);
               } else if(propertyName == "TriggerEnable"){
                  set_trigger_from_ODB(hDB, hBoardKey, b, false);
               } else if(propertyName.find("SwZero")==0){
                  set_swzerosuppression_from_ODB(hDB, hPropertyKey, propertyName, b->GetSerialNumber());
               } else if(propertyName != "MscbName" && propertyName != "Slot" && propertyName != "TriggerPrescaling" && propertyName != "TriggerDelay"){
                  std::string str = OdbToString(hDB, hPropertyKey);

                  b->AddProperty(propertyName, str);
                  //printf("\t%s\n", propertyName.c_str());
                  //printf("\t\t\"%s\"\n", str.c_str());

               }
            }
         }
      }
   }

   try{
      sys->Configure();
   } catch (const std::runtime_error& ex){
      cm_msg(MERROR, "reload_settings", "Configuration error: %s", ex.what());
      cm_disconnect_experiment();
      exit(0);
   }

   printf("DONE\n");

   // notify that the frontend is not busy any more
   config_busy = FALSE;
   db_set_value(hDB, hKeyRoot, "Variables/Config busy", &config_busy, sizeof(config_busy), 1, TID_BOOL);
   set_equipment_status("Trigger", "Ok", "greenLight");
}

/*- Alarm Callback ------------------------------------------------------*/

void alarm_callback(unsigned int id, const std::string &message)
{
   cm_msg(MERROR, "wdaq_alarm", message.c_str());

   switch(id){
   case WDAQLIB_ERROR_CORRUPTEDPACKET:
      al_trigger_alarm("DAQ Network", "Corrupted network packet", "DAQ Alarm", "", AT_INTERNAL);
      break;
   case WDAQLIB_ERROR_NOTBUILDING:
      al_trigger_alarm("DAQ Error", "Dropping all events", "DAQ Alarm", "", AT_INTERNAL);
      break; 
   case WDAQLIB_ERROR_MISSINGCALIB:
      al_trigger_alarm("DAQ Calibration", "Missing WDB calibration file", "DAQ Alarm", "", AT_INTERNAL);
      break; 
   case WDAQLIB_ERROR_CALIBTEMPERATURE:
      al_trigger_alarm("DAQ Temperature", "WDB Calibration temperature beyond specs", "DAQ Alarm", "", AT_INTERNAL);
      break; 
   case WDAQLIB_ERROR_PLLLOCK:
      al_trigger_alarm("DAQ PLL Lock", "WDB PLL is not locked", "DAQ Alarm", "", AT_INTERNAL);
      break; 
   case WDAQLIB_ERROR_TEMPERATURE:
      al_trigger_alarm("DAQ Overtemperature", "WDB temperature beyond specs", "DAQ Alarm", "", AT_INTERNAL);
      break; 
   }

}

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
   HNDLE hDB;
   HNDLE hKeyRoot;
   char str[256];
   int size = sizeof(str);

   printf("frontend_init: begin\n");
   set_equipment_status("Trigger", "Initializing ...", "yellowLight");

   cm_get_experiment_database(&hDB, NULL);

   //get Equipment odb key
   sprintf(str, "/Equipment/%s", equipment->name);
   db_find_key(hDB, 0, str, &hKeyRoot);

   // indicate that frontend is busy
   BOOL config_busy = TRUE;
   db_set_value(hDB, hKeyRoot, "Variables/Config busy", &config_busy, sizeof(config_busy), 1, TID_BOOL);

   //init XML string
   const char* onlinePath = getenv("ONLINECONF");
   if(onlinePath != NULL){
      sprintf(str, "%s/online.xml", onlinePath);
   } else {
      sprintf(str, "./online.xml");
   }
   int ret = db_get_value(hDB, hKeyRoot, "Settings/XML File", str, &size, TID_STRING, TRUE);
   printf("filename: %s\n", str);

   //Create ReloadAll key
   BOOL reload_all = FALSE;
   db_set_value(hDB, hKeyRoot, "Settings/Reload all", &reload_all, sizeof(reload_all), 1, TID_BOOL);
   HNDLE hReloadAllKey;
   db_find_key(hDB, hKeyRoot, "Settings/Reload all", &hReloadAllKey);

   db_watch(hDB, hReloadAllKey, reload_settings, nullptr);

   //create a new system from xml
   sys = new WDSystem();
   sys->CreateFromXml(str);
   try{
      sys->Connect();
   } catch (const std::runtime_error& ex){
      cm_msg(MERROR, "frontend_init", "Connect error: %s", ex.what());
      return FE_ERR_HW;
   }
   try{
      sys->Init();
   } catch (const std::runtime_error& ex){
      cm_msg(MERROR, "frontend_init", "Init error: %s", ex.what());
      return FE_ERR_HW;
   }

   //delete old WaveDAQ tree
   HNDLE hWaveDAQKey;
   sprintf(str, "Settings/WaveDAQ");
   ret = db_find_key(hDB, hKeyRoot, str, &hWaveDAQKey);
   if (ret == DB_SUCCESS) {
      //set mode to Read/Write/Delete
      ret = db_set_mode(hDB, hWaveDAQKey, MODE_READ | MODE_WRITE | MODE_DELETE, 1); 

      ret = db_delete_key(hDB, hWaveDAQKey, 0);
      if (ret != DB_SUCCESS){
         cm_msg(MERROR, "frontend_init", "Frontend error: Cannot delete key %s, db_delete_key() returned %d", str, ret);
         return FE_ERR_ODB ;
      }
   }

   //attach alarm callback
   sys->GetDAQSystem()->GetAlarms()->SetCallback(WDAQLIB_ERROR_CORRUPTEDPACKET, alarm_callback);
   sys->GetDAQSystem()->GetAlarms()->SetCallback(WDAQLIB_ERROR_NOTBUILDING, alarm_callback);
   sys->GetDAQSystem()->GetAlarms()->SetCallback(WDAQLIB_ERROR_MISSINGCALIB, alarm_callback);
   sys->GetDAQSystem()->GetAlarms()->SetCallback(WDAQLIB_ERROR_CALIBTEMPERATURE, alarm_callback);
   sys->GetDAQSystem()->GetAlarms()->SetCallback(WDAQLIB_ERROR_PLLLOCK, alarm_callback);
   sys->GetDAQSystem()->GetAlarms()->SetCallback(WDAQLIB_ERROR_TEMPERATURE, alarm_callback);

   //spawn threads
   sys->SetDaqProperty("NoWriter", "true");
   sys->SpawnDAQ();

   //add Midas readout thread
   unsigned int nReadoutThreads;
   try{
      nReadoutThreads = sys->GetDaqProperty("MidasThreads").GetUInt();
   } catch (const std::out_of_range& ex){
      nReadoutThreads = 1;
   }

   for(int iReadoutThread=0; iReadoutThread<nReadoutThreads; iReadoutThread++){
      printf("starting readout thread %d\n", iReadoutThread);
      //allocate midas ring buffer
      create_event_rb(iReadoutThread);

      //create threads
      MidasProducer *thread = new MidasProducer(sys->fCalibratedBuffer, sys, equipment, iReadoutThread, sys->GetDAQSystem());
      thread->Start();
      midasThreads.push_back(thread);
   }

   //train serdes
   try{
      sys->SetSerdesTraining(true);
      sys->TrainSerdes(false);
   } catch (const std::runtime_error& ex){
      cm_msg(MERROR, "frontend_init", "Serdes training error: %s", ex.what());
      return FE_ERR_HW;
   }

   //create from XML
   for(auto c : *sys){
      //create key with MscbName
      HNDLE hNameKey;
      sprintf(str, "Settings/WaveDAQ/%s/MscbName", c->GetCrateName().c_str());
      int ret = db_set_value(hDB, hKeyRoot, str, c->GetMscbName().c_str(), (c->GetMscbName().size()+1)*sizeof(char), 1, TID_STRING);
      if(ret != DB_SUCCESS){
         cm_msg(MERROR, "frontend_init", "Frontend error: Cannot create key %s, db_set_value() returned %d", str, ret);
         return FE_ERR_ODB ;
      }
      ret = db_find_key(hDB, hKeyRoot, str, &hNameKey);
      if (ret == DB_SUCCESS) {
         ret = db_set_mode(hDB, hNameKey, MODE_READ, 0); 
         if(ret != DB_SUCCESS){
            cm_msg(MERROR, "frontend_init", "Frontend error: Cannot lock key %s, db_set_mode() returned %d", str, ret);
            return FE_ERR_ODB ;
         }
      }

      //create key with CrateID
      HNDLE hCrateIDKey;
      sprintf(str, "Settings/WaveDAQ/%s/CrateID", c->GetCrateName().c_str());
      int crateid = c->GetCrateNumber();
      ret = db_set_value(hDB, hKeyRoot, str, &crateid, sizeof(int), 1, TID_INT);
      if(ret != DB_SUCCESS){
         cm_msg(MERROR, "frontend_init", "Frontend error: Cannot create key %s, db_set_value() returned %d", str, ret);
         return FE_ERR_ODB ;
      }
      ret = db_find_key(hDB, hKeyRoot, str, &hCrateIDKey);
      if (ret == DB_SUCCESS) {
         ret = db_set_mode(hDB, hCrateIDKey, MODE_READ, 0); 
         if(ret != DB_SUCCESS){
            cm_msg(MERROR, "frontend_init", "Frontend error: Cannot lock key %s, db_set_mode() returned %d", str, ret);
            return FE_ERR_ODB ;
         }
      }

      for(auto b :*c)
         if(b){
            //create key with Slot
            HNDLE hSlotKey;
            sprintf(str, "Settings/WaveDAQ/%s/%s/Slot", c->GetCrateName().c_str(), b->GetBoardName().c_str());
            char slot = b->GetSlot();
            int ret = db_set_value(hDB, hKeyRoot, str, &slot, sizeof(char), 1, TID_BYTE);
            if(ret != DB_SUCCESS){
               cm_msg(MERROR, "frontend_init", "Frontend error: Cannot create key %s, db_set_value() returned %d", str, ret);
               return FE_ERR_ODB ;
            }
            ret = db_find_key(hDB, hKeyRoot, str, &hSlotKey);
            if (ret == DB_SUCCESS) {
               ret = db_set_mode(hDB, hSlotKey, MODE_READ, 0); 
               if(ret != DB_SUCCESS){
                  cm_msg(MERROR, "frontend_init", "Frontend error: Cannot lock key %s, db_set_mode() returned %d", str, ret);
                  return FE_ERR_ODB ;
               }
            }

            if(dynamic_cast<WDWDB*>(b)!=nullptr){
               //Create key with MscbName
               sprintf(str, "Settings/WaveDAQ/%s/%s/MscbName", c->GetCrateName().c_str(), b->GetBoardName().c_str());
               int ret = db_set_value(hDB, hKeyRoot, str, static_cast<WDWDB*>(b)->GetName().c_str(), (static_cast<WDWDB*>(b)->GetName().size()+1)*sizeof(char), 1, TID_STRING);
               if(ret != DB_SUCCESS){
                  cm_msg(MERROR, "frontend_init", "Frontend error: Cannot create key %s, db_set_value() returned %d", str, ret);
                  return FE_ERR_ODB ;
               }
               ret = db_find_key(hDB, hKeyRoot, str, &hNameKey);
               if (ret == DB_SUCCESS) {
                  ret = db_set_mode(hDB, hNameKey, MODE_READ, 0); 
                  if(ret != DB_SUCCESS){
                     cm_msg(MERROR, "frontend_init", "Frontend error: Cannot lock key %s, db_set_mode() returned %d", str, ret);
                     return FE_ERR_ODB ;
                  }
               }

               //Add Properties
               float frontend_gain[16];
               try{
                  const float* vals = nullptr;
                  int64_t size;
                  vals = b->GetProperty("FrontendGain").GetFloatVector(&size);

                  if(size == 1){
                     for(int i=0; i<16; i++) frontend_gain[i] = vals[0];
                  } else if(size==16){
                     for(int i=0; i<16; i++) frontend_gain[i] = vals[i];
                  } 
               } catch (const std::runtime_error& ex){
                     for(int i=0; i<16; i++) frontend_gain[i] = static_cast<WDWDB*>(b)->GetFeGain(i);
               }
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "FrontendGain", frontend_gain, sizeof(float)*16, 16, TID_FLOAT);
               if(ret != SUCCESS) return ret;

               BOOL frontend_pzc[16];
               try{
                  int64_t size;
                  const int* vals = b->GetProperty("FrontendPzc").GetIntVector(&size);

                  if(size == 1){
                     for(int i=0; i<16; i++) if(vals[0]==1) frontend_pzc[i] = TRUE; else frontend_pzc[i] = FALSE;
                  } else if(size==16){
                     for(int i=0; i<16; i++) if(vals[i]==1) frontend_pzc[i] = TRUE; else frontend_pzc[i] = FALSE;
                  } 
               } catch (const std::runtime_error& ex){
                  for(int i=0; i<16; i++) frontend_pzc[i] = (static_cast<WDWDB*>(b)->GetFePzc(i))?TRUE:FALSE;
               }
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "FrontendPzc", frontend_pzc, sizeof(BOOL)*16, 16, TID_BOOL);
               if(ret != SUCCESS) return ret;

               int frontend_pzc_level;
               try{
                  frontend_pzc_level = b->GetProperty("FrontendPzcLevel").GetInt();
               } catch (const std::runtime_error& ex){
                  frontend_pzc_level = static_cast<WDWDB*>(b)->GetDacPzcLevelN()+1;
               }
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "FrontendPzcLevel", &frontend_pzc_level, sizeof(int), 1, TID_INT);
               if(ret != SUCCESS) return ret;

               int sampling_frequency;
               try{
                  sampling_frequency = b->GetProperty("SamplingFrequency").GetInt();
               } catch (const std::runtime_error& ex){
                  sampling_frequency = static_cast<WDWDB*>(b)->GetDrsSampleFreqMhz();
               }
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SamplingFrequency", &sampling_frequency, sizeof(int), 1, TID_INT);
               if(ret != SUCCESS) return ret;

               float trigger_level[16];
               try{
                  int64_t size;
                  const float* vals = b->GetProperty("TriggerLevel").GetFloatVector(&size);

                  if(size == 1){
                     for(int i=0; i<16; i++) trigger_level[i] = vals[0];
                  } else if(size==16){
                     for(int i=0; i<16; i++) trigger_level[i] = vals[i];
                  } 
               } catch (const std::runtime_error& ex){
                  for(int i=0; i<16; i++) trigger_level[i] = static_cast<WDWDB*>(b)->GetDacTriggerLevelV(i);
               }
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TriggerLevel", trigger_level, sizeof(float)*16, 16, TID_FLOAT);
               if(ret != SUCCESS) return ret;

               BYTE trigger_gain[16];
               try{
                  int64_t size;
                  const int* vals = b->GetProperty("TriggerGain").GetIntVector(&size);

                  if(size == 1){
                     for(int i=0; i<16; i++) trigger_gain[i] = vals[0];
                  } else if(size==16){
                     for(int i=0; i<16; i++) trigger_gain[i] = vals[i];
                  } 
               } catch (const std::runtime_error& ex){
                  for(int i=0; i<16; i++) trigger_gain[i] = -1;//FIXME: add readout from board
               }
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TriggerGain", trigger_gain, sizeof(BYTE)*16, 16, TID_SBYTE);
               if(ret != SUCCESS) return ret;

               WORD trigger_tdc_mask;
               try{
                  trigger_tdc_mask = b->GetProperty("TriggerTdcMask").GetUHex();
               } catch (const std::runtime_error& ex){
                  trigger_tdc_mask = static_cast<WDWDB*>(b)->GetAdvTrgTdcChMask();
               }
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TriggerTdcMask", &trigger_tdc_mask, sizeof(WORD), 1, TID_WORD);
               if(ret != SUCCESS) return ret;

               BYTE trigger_tdc_offset[16];
               try{
                  int64_t size;
                  const int* vals = b->GetProperty("TriggerTdcOffset").GetIntVector(&size);

                  if(size == 1){
                     for(int i=0; i<16; i++) trigger_tdc_offset[i] = vals[0];
                  } else if(size==16){
                     for(int i=0; i<16; i++) trigger_tdc_offset[i] = vals[i];
                  } 
               } catch (const std::runtime_error& ex){
                  for(int i=0; i<16; i++) trigger_tdc_offset[i] = 0;//FIXME: add readout from board
               }
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TriggerTdcOffset", trigger_tdc_offset, sizeof(BYTE)*16, 16, TID_BYTE);
               if(ret != SUCCESS) return ret;

               DWORD drs_transmit_enable;
               try{
                  drs_transmit_enable = b->GetProperty("DRSChannelTxEnable").GetUHex();
               } catch (const std::runtime_error& ex){
                  drs_transmit_enable = static_cast<WDWDB*>(b)->GetDrsChTxEn();
               }
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "DRSChannelTxEnable", &drs_transmit_enable, sizeof(DWORD), 1, TID_DWORD);
               if(ret != SUCCESS) return ret;

               WORD adc_transmit_enable;
               try{
                  adc_transmit_enable = b->GetProperty("ADCChannelTxEnable").GetUHex();
               } catch (const std::runtime_error& ex){
                  adc_transmit_enable = static_cast<WDWDB*>(b)->GetAdcChTxEn();
               }
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "ADCChannelTxEnable", &adc_transmit_enable, sizeof(WORD), 1, TID_WORD);
               if(ret != SUCCESS) return ret;

               WORD tdc_transmit_enable;
               try{
                  tdc_transmit_enable = b->GetProperty("TDCChannelTxEnable").GetUHex();
               } catch (const std::runtime_error& ex){
                  tdc_transmit_enable = static_cast<WDWDB*>(b)->GetTdcChTxEn();
               }
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TDCChannelTxEnable", &tdc_transmit_enable, sizeof(WORD), 1, TID_WORD);
               if(ret != SUCCESS) return ret;

               BOOL trg_transmit_enable;
               try{
                   bool val = b->GetProperty("TRGTxEnable").GetBool();

                  trg_transmit_enable = (val)?TRUE:FALSE;
               } catch (const std::runtime_error& ex){
                  trg_transmit_enable = static_cast<WDWDB*>(b)->GetTrgTxEn();
               }
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TRGTxEnable", &trg_transmit_enable, sizeof(BOOL), 1, TID_BOOL);
               if(ret != SUCCESS) return ret;

               BOOL zero_suppression_enable;
               try{
                   bool val = b->GetProperty("ZeroSuppressionEnable").GetBool();

                  zero_suppression_enable = (val)?TRUE:FALSE;
               } catch (const std::runtime_error& ex){
                  zero_suppression_enable = static_cast<WDWDB*>(b)->GetZeroSuprEn();
               }
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "ZeroSuppressionEnable", &zero_suppression_enable, sizeof(BOOL), 1, TID_BOOL);
               if(ret != SUCCESS) return ret;

               WORD sw_zero_prescaling = 0;
               WORD sw_zero_clk_roi_start = 0;
               WORD sw_zero_clk_roi_end = 1023;
               WORD sw_zero_signal_roi_start = 0;
               WORD sw_zero_signal_roi_end = 1023;
               WORD sw_zero_region_start = 0;
               WORD sw_zero_region_end = 1023;
               WORD sw_zero_rebin_factor = 0;
               float sw_zero_rebin_signal_thr = 0;
               float sw_zero_wfm_suppression_thr = 0;
               float sw_zero_rebin_thr[MidasZeroSuppressionNRebinThr] = {0};
               WORD sw_zero_adc_roi_start = 0;
               WORD sw_zero_adc_roi_end = 2047;
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SwZeroSuppressionPrescaling", &sw_zero_prescaling, sizeof(WORD), 1, TID_WORD);
               if(ret != SUCCESS) return ret;
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SwZeroSuppressionClkROIStart", &sw_zero_clk_roi_start, sizeof(WORD), 1, TID_WORD);
               if(ret != SUCCESS) return ret;
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SwZeroSuppressionClkROIEnd", &sw_zero_clk_roi_end, sizeof(WORD), 1, TID_WORD);
               if(ret != SUCCESS) return ret;
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SwZeroSuppressionSignalROIStart", &sw_zero_signal_roi_start, sizeof(WORD), 1, TID_WORD);
               if(ret != SUCCESS) return ret;
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SwZeroSuppressionSignalROIEnd", &sw_zero_signal_roi_end, sizeof(WORD), 1, TID_WORD);
               if(ret != SUCCESS) return ret;
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SwZeroSuppressionRegionStart", &sw_zero_region_start, sizeof(WORD), 1, TID_WORD);
               if(ret != SUCCESS) return ret;
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SwZeroSuppressionRegionEnd", &sw_zero_region_end, sizeof(WORD), 1, TID_WORD);
               if(ret != SUCCESS) return ret;
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SwZeroSuppressionRebinFactor", &sw_zero_rebin_factor, sizeof(WORD), 1, TID_WORD);
               if(ret != SUCCESS) return ret;
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SwZeroSuppressionRebinSignalThr", &sw_zero_rebin_signal_thr, sizeof(float), 1, TID_FLOAT);
               if(ret != SUCCESS) return ret;
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SwZeroSuppressionThr", &sw_zero_wfm_suppression_thr, sizeof(float), 1, TID_FLOAT);
               if(ret != SUCCESS) return ret;
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SwZeroSuppressionRebinThr", &sw_zero_rebin_thr, sizeof(float)*MidasZeroSuppressionNRebinThr, MidasZeroSuppressionNRebinThr, TID_FLOAT);
               if(ret != SUCCESS) return ret;
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SwZeroSuppressionAdcROIStart", &sw_zero_adc_roi_start, sizeof(WORD), 1, TID_WORD);
               if(ret != SUCCESS) return ret;
               ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "SwZeroSuppressionAdcROIEnd", &sw_zero_adc_roi_end, sizeof(WORD), 1, TID_WORD);
               if(ret != SUCCESS) return ret;

               for(auto thread: midasThreads){
                  thread->SetSwZeroSuppression(b->GetSerialNumber(), sw_zero_prescaling, sw_zero_clk_roi_start, sw_zero_clk_roi_end, sw_zero_signal_roi_start, sw_zero_signal_roi_end, sw_zero_region_start, sw_zero_region_end, sw_zero_rebin_factor, sw_zero_rebin_signal_thr, sw_zero_wfm_suppression_thr, sw_zero_rebin_thr, sw_zero_adc_roi_start, sw_zero_adc_roi_end);
               }

               //debug properties to be used only by experts
               if (verbosity_level) {
                  //interpacket delay
                  DWORD ipd;
                  try{
                     ipd = b->GetProperty("IPD").GetUHex();
                  } catch (const std::runtime_error& ex){
                     ipd = static_cast<WDWDB*>(b)->GetInterPkgDelay();
                  }
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "IPD", &ipd, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;

                  //first packet delay
                  DWORD fpd;
                  try{
                     fpd = b->GetProperty("FPD").GetUHex();
                  } catch (const std::runtime_error& ex){
                     fpd = static_cast<WDWDB*>(b)->GetFirstPkgDly();
                  }
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "FPD", &fpd, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;

                  //Pedestal subtraction configuration: threshold
                  WORD trigger_pedestal_threshold;
                  try{
                     trigger_pedestal_threshold = b->GetProperty("TriggerPedestalThreshold").GetUHex();
                  } catch (const std::runtime_error& ex){
                     trigger_pedestal_threshold = static_cast<WDWDB*>(b)->GetAdvTrgPedCfg() & 0xFFFF;
                  }
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TriggerPedestalThreshold", &trigger_pedestal_threshold, sizeof(WORD), 1, TID_WORD);
                  if(ret != SUCCESS) return ret;

                  //Pedestal subtraction configuration: delay
                  WORD trigger_pedestal_delay;
                  try{
                     trigger_pedestal_delay = b->GetProperty("TriggerPedestalDelay").GetUHex();
                  } catch (const std::runtime_error& ex){
                     trigger_pedestal_delay = (static_cast<WDWDB*>(b)->GetAdvTrgPedCfg()>>16) & 0x3F;
                  }
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TriggerPedestalDelay", &trigger_pedestal_delay, sizeof(WORD), 1, TID_WORD);
                  if(ret != SUCCESS) return ret;

                  //Pedestal subtraction configuration: Adder selection
                  WORD trigger_pedestal_addersel;
                  try{
                     trigger_pedestal_addersel = b->GetProperty("TriggerPedestalAddersel").GetUHex();
                  } catch (const std::runtime_error& ex){
                     trigger_pedestal_addersel = (static_cast<WDWDB*>(b)->GetAdvTrgPedCfg()>>24) & 0x3;
                  }
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TriggerPedestalAddersel", &trigger_pedestal_addersel, sizeof(WORD), 1, TID_WORD);
                  if(ret != SUCCESS) return ret;

                  // Scaler transmit enable
                  BOOL scaler_transmit_enable;
                  try{
                     bool val = b->GetProperty("ScalerTxEnable").GetBool();

                     scaler_transmit_enable = (val)?TRUE:FALSE;
                  } catch (const std::runtime_error& ex){
                     scaler_transmit_enable = static_cast<WDWDB*>(b)->GetSclTxEn();
                  }
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "ScalerTxEnable", &scaler_transmit_enable, sizeof(BOOL), 1, TID_BOOL);
                  if(ret != SUCCESS) return ret;

               }


            } else if(dynamic_cast<WDTCB*>(b)!=nullptr){
               try{
                  int64_t size, prescalingSize;
                  const int* enables = b->GetProperty("TriggerEnable").GetIntVector(&size);
                  const unsigned int* prescaling = b->GetProperty("TriggerPrescaling").GetUIntVector(&prescalingSize);
                  if(size != prescalingSize) return FE_ERR_DRIVER;

                  BOOL arrEna[64];
                  for(int i=0; i<64; i++) arrEna[i]=FALSE;
                  for(int i=0; i<size; i++) arrEna[enables[i]]=TRUE;
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TriggerEnable", arrEna, sizeof(BOOL)*64, 64, TID_BOOL);
                  if(ret != SUCCESS) return ret;

                  DWORD arr[64];
                  for(int i=0; i<64; i++) arr[i]=0;
                  for(int i=0; i<size; i++) arr[enables[i]] = prescaling[i];
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TriggerPrescaling", arr, sizeof(DWORD)*64, 64, TID_DWORD);
                  if(ret != SUCCESS) return ret;

                  try{
                     int64_t delaySize;
                     const unsigned int* delay = b->GetProperty("TriggerDelay").GetUIntVector(&delaySize);
                     if(size != delaySize) return FE_ERR_DRIVER;

                     for(int i=0; i<64; i++) arr[i]=0;
                     for(int i=0; i<size; i++) arr[enables[i]] = delay[i];
                     ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TriggerDelay", arr, sizeof(INT)*64, 64, TID_INT);
                     if(ret != SUCCESS) return ret;
                  } catch (const std::runtime_error& ex){
                  }
               } catch (const std::runtime_error& ex){
               }

               try{
                  int64_t size;
                  const unsigned int* dlys = b->GetProperty("DetectorDelay").GetUIntVector(&size);
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "DetectorDelay", dlys, sizeof(INT)*size, size, TID_INT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  int64_t size;
                  const unsigned int* dlys = b->GetProperty("TdcDelay").GetUIntVector(&size);
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TdcDelay", dlys, sizeof(INT)*size, size, TID_INT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  int64_t size;
                  const unsigned int* dlys = b->GetProperty("CombinedConditionDelay").GetUIntVector(&size);
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "CombinedConditionDelay", dlys, sizeof(INT)*size, size, TID_INT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("TimeNarrowThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TimeNarrowThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("TimeWideThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TimeWideThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("XecHighThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecHighThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("XecLowThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecLowThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("XecVetoThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecVetoThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("XecPatchId").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecPatchId", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("XecMppcNTdcThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecMppcNTdcThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("XecPmtNTdcThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecPmtNTdcThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("XecMppcWaveformThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecMppcWaveformThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("XecPatchDelay").GetUInt();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecPatchDelay", &thr, sizeof(INT), 1, TID_INT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("XecAlfaThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecAlfaThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const float thr = b->GetProperty("XecAlfaScale").GetFloat();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecAlfaScale", &thr, sizeof(float), 1, TID_FLOAT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const bool thr = b->GetProperty("XecMovingAverage").GetBool();
                  DWORD val;
                  if(thr){
                     val = TRUE;
                  } else {
                     val = FALSE;
                  }
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecMovingAverage", &val, sizeof(DWORD), 1, TID_BOOL);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const bool thr = b->GetProperty("XecTimeFromPmt").GetBool();
                  DWORD val;
                  if(thr){
                     val = TRUE;
                  } else {
                     val = FALSE;
                  }
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecTimeFromPmt", &val, sizeof(DWORD), 1, TID_BOOL);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }
               
               try{
                  const bool thr = b->GetProperty("XecMaxFromQsum").GetBool();
                  DWORD val;
                  if(thr){
                     val = TRUE;
                  } else {
                     val = FALSE;
                  }
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecMaxFromQsum", &val, sizeof(DWORD), 1, TID_BOOL);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }
               
               try{
                  const int val = b->GetProperty("XecPmtGlobalGain").GetUInt();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "XecPmtGlobalGain", &val, sizeof(int), 1, TID_INT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  int64_t size;
                  const unsigned int* thr = b->GetProperty("TcMask").GetUHexVector(&size);
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TcMask", thr, sizeof(DWORD)*size, size, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const int thr = b->GetProperty("TcMultiplicityThreshold").GetInt();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TcMultiplicityThreshold", &thr, sizeof(INT), 1, TID_INT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const int thr = b->GetProperty("TcTrackMultiplicityThreshold").GetInt();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TcTrackMultiplicityThreshold", &thr, sizeof(INT), 1, TID_INT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const int thr = b->GetProperty("TcTrackTimeThreshold").GetInt();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TcTrackTimeThreshold", &thr, sizeof(INT), 1, TID_INT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const int offset = b->GetProperty("TcTimeOffset").GetInt();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "TcTimeOffset", &offset, sizeof(INT), 1, TID_INT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  int64_t size;
                  const unsigned int* thr = b->GetProperty("CdchMask").GetUHexVector(&size);
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "CdchMask", thr, sizeof(DWORD)*size, size, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("CdchTriggerMask").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "CdchTriggerMask", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const int thr = b->GetProperty("CdchUSMultiplicityThreshold").GetInt();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "CdchUSMultiplicityThreshold", &thr, sizeof(INT), 1, TID_INT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const int thr = b->GetProperty("CdchDSMultiplicityThreshold").GetInt();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "CdchDSMultiplicityThreshold", &thr, sizeof(INT), 1, TID_INT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("BgoThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "BgoThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("BgoVetoThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "BgoVetoThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  int64_t size;
                  const int* thr = b->GetProperty("BgoHitDelay").GetIntVector(&size);
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "BgoHitDelay", thr, sizeof(INT)*size, size, TID_INT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("BgoTriggerMask").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "BgoTriggerMask", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("RdcThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "RdcThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("RdcVetoThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "RdcVetoThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  int64_t size;
                  const unsigned int* masks = b->GetProperty("RdcHitMask").GetUHexVector(&size);
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "RdcHitMask", masks, sizeof(DWORD)*size, size, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("RdcHitDelay").GetUInt();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "RdcHitDelay", &thr, sizeof(INT), 1, TID_INT);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("RdcTriggerMask").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "RdcTriggerMask", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("CrcHitMask").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "CrcHitMask", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("CrcPairMask").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "CrcPairMask", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("NgenDelay").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "NgenDelay", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("NgenWidth").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "NgenWidth", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("NgenHighThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "NgenHighThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
                  const unsigned int thr = b->GetProperty("NgenLowThreshold").GetUHex();
                  ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "NgenLowThreshold", &thr, sizeof(DWORD), 1, TID_DWORD);
                  if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }

               try{
		 const unsigned int mask = b->GetProperty("LoLXMask").GetUHex();
		 ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "LoLXMask", &mask, sizeof(DWORD), 1, TID_DWORD);
		 if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }
	       
	       try{
		 const unsigned int mult = b->GetProperty("LoLXMajValScin").GetUHex();
		 ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "LoLXMajValScin", &mult, sizeof(DWORD), 1, TID_DWORD);
		 if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }
	       
	       
	       try{
		 const unsigned int mult = b->GetProperty("LoLXMajValCher").GetUHex();
		 ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "LoLXMajValCher", &mult, sizeof(DWORD), 1, TID_DWORD);
		 if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }
	       
               try{
		 const unsigned int mult = b->GetProperty("LoLXMajValBare").GetUHex();
		 ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), "LoLXMajValBare", &mult, sizeof(DWORD), 1, TID_DWORD);
		 if(ret != SUCCESS) return ret;
               } catch (const std::runtime_error& ex){
               }
	       

               //generate bank readout enables
               try {
                  std::stringstream ss;
                  ss << b->GetProperty("Banks").GetStringValue();
                  std::string token;
                  while(std::getline(ss, token, ',')) {
                     std::string fieldName = "Write "+token;

                     BOOL val = TRUE;
                     ret = AddToOdb(hDB, hKeyRoot, c->GetCrateName(), b->GetBoardName(), fieldName.c_str(), &val, sizeof(DWORD), 1, TID_BOOL);
                     if(ret != SUCCESS) return ret;
                  }

               } catch (const std::runtime_error& ex){
               }
            }
         }

      HNDLE hCrateKey;
      sprintf(str, "Settings/WaveDAQ/%s", c->GetCrateName().c_str());
      ret = db_find_key(hDB, hKeyRoot, str, &hCrateKey);
      if (ret != DB_SUCCESS){
         cm_msg(MERROR, "frontend_init", "Frontend error: Cannot find key %s, db_find_key() returned %d", str, ret);
         return FE_ERR_ODB ;
      }

      //watch crate odb record
      ret = db_watch(hDB, hCrateKey, crate_settings_changed, nullptr);
      if (ret != DB_SUCCESS){
         cm_msg(MERROR, "frontend_init", "Frontend error: Cannot watch key %s, db_watch() returned %d", str, ret);
         return FE_ERR_ODB ;
      }

   }

   //wait for serdes training to finish
   sys->WaitSerdesTrainingFinish();
   printf("Serdes State: ");
   if(sys->IsSerdesGood()){
      printf("OK!\n");
   } else {
      printf("***BAD***!\n");
      cm_msg(MERROR, "frontend_init", "Cannot calibrate serial links");
      return FE_ERR_HW;
   }

   //configure
   try{
      sys->Configure();
   } catch (const std::runtime_error& ex){
      cm_msg(MERROR, "frontend_init", "Configuration error: %s", ex.what());
      return FE_ERR_HW;
   }
   
   //Notify to be ready
   set_equipment_status("Trigger", "Ok", "greenLight");
   config_busy = FALSE;
   db_set_value(hDB, hKeyRoot, "Variables/Config busy", &config_busy, sizeof(config_busy), 1, TID_BOOL);

   //reset errors
   sys->GetDAQSystem()->GetAlarms()->Clean();

   threadStatTime = ss_millitime();//reset timer for buffer history

   //reset trigger counters
   float rates[64];
   float ltfraction = 0;
   for(int i = 0; i<64; i++)
      rates[i] = 0;
   db_set_value(hDB, hKeyRoot, "Variables/Trigger Rate", &rates, sizeof(rates), 64, TID_FLOAT);
   db_set_value(hDB, hKeyRoot, "Variables/Livetime", &ltfraction, sizeof(ltfraction), 1, TID_FLOAT);

   std::cout << "OK" << std::endl;

   return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
   return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
   //reset errors
   sys->GetDAQSystem()->GetAlarms()->Clean();

   // set trgcouprev ttimeprev and ltimeprev at 0;
   for(int i = 0; i<64; i++)
      trgcouprev[i] = 0;
   ttimeprev = 0;
   ltimeprev = 0;

   sys->GoRun();
   return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
   sys->StopRun();

   return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
   /* if frontend_call_loop is true, this routine gets called when
      the frontend is idle or once between every event */

   DWORD currentTime = ss_millitime();

   //log thread statistics every 10 second 
   if((currentTime - threadStatTime) > 10000) {
      //get ODB handle ODB
      HNDLE hKeyRoot;
      char str[256];
      sprintf(str, "/Equipment/%s/Variables", equipment->name);
      db_find_key(hDB, 0, str, &hKeyRoot);

      //packet buffer
      for(int i=0; i<sys->fPacketBuffer->GetNBuffers(); i++){
         auto buff = sys->fPacketBuffer->GetBufferAt(i);
         float occupancy = buff->GetOccupancy();
         unsigned int nevents = occupancy * buff->GetMaxSize();
         sprintf(str, "Buffer/PacketBuffer%02d", i);
         db_set_value(hDB, hKeyRoot, str, &nevents, sizeof(nevents), 1, TID_UINT32);
      }

      //builder buffer
      unsigned long nBuilders = sys->fBuilderThreads.size();
      for(int i=0; i<nBuilders; i++){
         unsigned int eventsInQueue = sys->fBuilderThreads[i]->GetEventsInQueue();
         sprintf(str, "Buffer/BuilderBuffer%02d", i);
         db_set_value(hDB, hKeyRoot, str, &eventsInQueue, sizeof(eventsInQueue), 1, TID_UINT32);
      }

      //event buffer
      float eventOccupancy = sys->fEventBuffer->GetOccupancy();
      unsigned int events = eventOccupancy * sys->fEventBuffer->GetMaxSize();
      db_set_value(hDB, hKeyRoot, "Buffer/EventBuffer", &events, sizeof(events), 1, TID_UINT32);

      float calibratedOccupancy = sys->fCalibratedBuffer->GetOccupancy();
      unsigned int calibrated = calibratedOccupancy * sys->fCalibratedBuffer->GetMaxSize();
      db_set_value(hDB, hKeyRoot, "Buffer/CalibratedBuffer", &calibrated, sizeof(calibrated), 1, TID_UINT32);

      threadStatTime = currentTime;

      //midas buffers
      for(int iMidasBuffer=0; get_event_rbh(iMidasBuffer); iMidasBuffer++){
         int nbytes;
         int status = rb_get_buffer_level(get_event_rbh(iMidasBuffer), &nbytes);
         if(status == DB_SUCCESS){
            sprintf(str, "Buffer/MidasBuffer%02d", iMidasBuffer);
            float occupancy = nbytes * 1. /event_buffer_size;
            db_set_value(hDB, hKeyRoot, str, &occupancy, sizeof(occupancy), 1, TID_FLOAT);
         }
      }

      //log thread statistics
      unsigned int nDroppedPackets = 0;
      for(auto thread : sys->fCollectorThreads){
         nDroppedPackets += thread->GetDroppedPackets();
      }
      db_set_value(hDB, hKeyRoot, "Thread/DroppedPackets", &nDroppedPackets, sizeof(nDroppedPackets), 1, TID_UINT32);

      unsigned int nDroppedBuiltEvents = 0;
      unsigned int nOldBuiltEvents = 0;
      for(auto thread : sys->fBuilderThreads){
         nDroppedBuiltEvents += thread->GetDroppedEvents();
         nOldBuiltEvents += thread->GetOldEvents();
      }
      db_set_value(hDB, hKeyRoot, "Thread/DroppedBuiltEvents", &nDroppedBuiltEvents, sizeof(nDroppedBuiltEvents), 1, TID_UINT32);
      db_set_value(hDB, hKeyRoot, "Thread/OldBuiltEvents", &nOldBuiltEvents, sizeof(nOldBuiltEvents), 1, TID_UINT32);

      unsigned int nDroppedCalibratedEvents = 0;
      for(auto thread : sys->fWorkerThreads){
         nDroppedCalibratedEvents += thread->GetDroppedEvents();
      }
      db_set_value(hDB, hKeyRoot, "Thread/DroppedCalibratedEvents", &nDroppedCalibratedEvents, sizeof(nDroppedCalibratedEvents), 1, TID_UINT32);

      unsigned int nDroppedMidasEvents = 0;
      for(auto thread : midasThreads){
         nDroppedMidasEvents += thread->GetDroppedEvents();
      }
      db_set_value(hDB, hKeyRoot, "Thread/DroppedMidasEvents", &nDroppedMidasEvents, sizeof(nDroppedMidasEvents), 1, TID_UINT32);
   }

   return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   //Not used

   return FALSE;
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   switch (cmd) {
      case CMD_INTERRUPT_ENABLE:
         break;
      case CMD_INTERRUPT_DISABLE:
         break;
      case CMD_INTERRUPT_ATTACH:
         break;
      case CMD_INTERRUPT_DETACH:
         break;
   }
   return SUCCESS;
}

/*-- Trigger event readout ----------------------------------------*/

INT read_scaler_event(char *pevent, INT off){
   //option for parallel scaler readout
   /*unsigned char *puchar;
   std::vector<uint64_t> scalers;

   // init bank structure
   bk_init32(pevent);

   //scaler readout from WDB
   bk_create(pevent, "SCLR", TID_BYTE, (void **)&puchar);
   for(auto &c : *sys){
      //  loop on WDB
      for(int i=0; i<16; i++){
         if(c->HasBoardIn(i)) {
            WDWDB* b = dynamic_cast<WDWDB*>(c->GetBoardAt(i));
            if(b != nullptr){
               scalers.clear();
               b->GetScalers(scalers, true);

               int crate = c->GetCrateNumber();
               int slot = b->GetSlot();
               int drs_voltage_flags = DRSH_VERSION4_2 | DRSH_ENC_MODE15;

               uint64_t* ptr = (uint64_t*)(puchar + sizeof(DRS_CHANNELV_HEADER));
               *(ptr++) = 0; //timestamp...

               for(int iSca=0; iSca<16; iSca++){
                  *(ptr++) = scaler[iSca];
               }

               drs_compose_channelv_header(puchar, drs_voltage_flags, crate, slot,
                                           0, 0, 17*sizeof(unsigned long), 0);
               //increment readout pointer
               puchar += sizeof(DRS_CHANNELV_HEADER) + 17*sizeof(unsigned long);
            }
         }
      }
   }
   bk_close(pevent, puchar);*/

   uint32_t triggercounters[66];
   float rates[64];
   float ltfraction;

   WDTCB* triggerb = static_cast <WDTCB*>(sys->GetTriggerBoard());

   triggerb->ReadCounters(triggercounters);

   u_int32_t ttime = triggercounters[64];
   u_int32_t ltime = triggercounters[65];

   if(ttime == ttimeprev || ltime == ltimeprev){
      return 0;
   }

   float ttimeFactor = 1.e6/ (ttime-ttimeprev);
   float ltimeFactor = 1.e6/ (ltime-ltimeprev);

   //Calculate rates
   for(int i = 0; i<64; i++){
      if(TCOUMODE[i] == true) {
         rates[i] = (triggercounters[i]-trgcouprev[i]) * ttimeFactor;
      } else {
         rates[i] = (triggercounters[i]-trgcouprev[i]) * ltimeFactor;
      }
      //update old counter
      trgcouprev[i] = triggercounters[i];
   }

   //update old totaltime and livetime
   ttimeprev = ttime;
   ltimeprev = ltime;

   //save to ODB
   HNDLE hKeyRoot;
   char str[256];
   sprintf(str, "/Equipment/%s", equipment->name);
   db_find_key(hDB, 0, str, &hKeyRoot);
   db_set_value(hDB, hKeyRoot, "Variables/Trigger Rate", &rates, sizeof(rates), 64, TID_FLOAT);

   ltfraction =  ttimeFactor/ltimeFactor;
   db_set_value(hDB, hKeyRoot, "Variables/Livetime", &ltfraction, sizeof(ltfraction), 1, TID_FLOAT);

   return 0;
}
